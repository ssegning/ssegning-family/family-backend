# Family Backend

### Helm

```bash
helm package .
```

```bash
curl --data-binary "@family-backend-0.1.9.tgz" https://charts.ssegning.com/ssegning/api/charts
```

```bash
helm dependency update family-backend
```

```bash
helm dependency build family-backend
```

```bash
helm install family-backend family-backend -n family
```

```bash
helm upgrade family-backend ./family-backend -n family
```
