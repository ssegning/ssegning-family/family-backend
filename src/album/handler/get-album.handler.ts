import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { Album } from '../entities/album.entity';
import { AlbumService } from '../album.service';
import { GetAlbumQuery } from '../model/get-album.query';

@QueryHandler(GetAlbumQuery)
export class GetAlbumHandler implements IQueryHandler<GetAlbumQuery, Album> {
  constructor(private readonly albumService: AlbumService) {
  }

  async execute(query: GetAlbumQuery): Promise<Album> {
    return this.albumService.findOne(query.id);
  }
}
