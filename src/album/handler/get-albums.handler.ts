import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { Album } from '../entities/album.entity';
import { AlbumService } from '../album.service';
import { GetAlbumsQuery } from '@album/model/get-albums.query';

@QueryHandler(GetAlbumsQuery)
export class GetAlbumsHandler
  implements IQueryHandler<GetAlbumsQuery, Album[]> {
  constructor(private readonly albumService: AlbumService) {
  }

  async execute(query: GetAlbumsQuery): Promise<Album[]> {
    return this.albumService.findList(query.ids, query.offset, query.size);
  }
}
