import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateAlbumInput } from './dto/create-album.input';
import { UpdateAlbumInput } from './dto/update-album.input';
import { Album } from './entities/album.entity';
import { InjectModel } from '@nestjs/mongoose';
import mongoose, { Model } from 'mongoose';
import { AlbumDb, AlbumDocument } from './database/album.db';

@Injectable()
export class AlbumService {
  constructor(
    @InjectModel(AlbumDb.name)
    private albumModel: Model<AlbumDocument>,
  ) {}

  async create(input: CreateAlbumInput): Promise<Album> {
    const dto = AlbumDb.mapInput(input);
    const model = new this.albumModel(dto);
    const createdModel = await model.save();
    return Album.map(createdModel);
  }

  async findAll(familyId: string): Promise<Album[]> {
    const models = await this.albumModel.find({ familyId }).exec();
    return models.map(Album.map);
  }

  async findList(
    ids: string[],
    offset: number,
    size: number,
  ): Promise<Album[]> {
    const list = await this.albumModel
      .find()
      .where('_id')
      .in(ids.map((id) => mongoose.Types.ObjectId(id)))
      .skip(offset)
      .limit(size)
      .exec();
    return list.map(Album.map);
  }

  async findOne(id: string): Promise<Album> {
    const model = await this.findById(id);
    return Album.map(model);
  }

  async update(id: string, input: Omit<UpdateAlbumInput, 'id'>) {
    const model = await this.findById(id);
    model.title = input.title;
    model.description = input.description;
    model.tags = input.tags;

    const newModel = await model.save();
    return Album.map(newModel);
  }

  async remove(id: string): Promise<void> {
    const model = await this.findById(id);
    await model.deleteOne();
  }

  private async findById(id: string) {
    const model = await this.albumModel.findById(id).exec();
    if (!model) {
      throw new NotFoundException('album.not.found');
    }
    return model;
  }
}
