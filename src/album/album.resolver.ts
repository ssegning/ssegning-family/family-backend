import {
  Args,
  Int,
  Mutation,
  Parent,
  Query,
  ResolveField,
  Resolver,
} from '@nestjs/graphql';
import { AlbumService } from './album.service';
import { Album } from './entities/album.entity';
import { CreateAlbumInput } from './dto/create-album.input';
import { UpdateAlbumInput } from './dto/update-album.input';
import { QueryBus } from '@nestjs/cqrs';
import { Account } from '@account/entities/account.entity';
import { Media } from '@media/entities/media.entity';
import { AlbumFavorite } from '@album-favorite/entities/album-favorite.entity';
import { GetAlbumMediasQuery } from '@media/model/get-album-medias.query';
import { GetAlbumFavoritesQuery } from '@album-favorite/model/get-album-favorites.query';
import { GetAccountQuery } from '@account/model/get-account.query';
import { GqlHeader } from '@shared/decorator/gql-header.decorator';
import { FAMILY_HEADER } from '@shared/constants/family-header-name';
import { GetFamilyPipe } from '@shared/pipe/get-family.pipe';
import { Family } from '@family/entities/family.entity';

@Resolver(() => Album)
export class AlbumResolver {
  constructor(
    private readonly albumService: AlbumService,
    private readonly queryBus: QueryBus,
  ) {}

  @Mutation(() => Album)
  createAlbum(
    @Args('input') input: CreateAlbumInput,
    @GqlHeader(FAMILY_HEADER, GetFamilyPipe) family: Family,
  ): Promise<Album> {
    input.familyId = family.id;
    return this.albumService.create(input);
  }

  @Query(() => [Album], { name: 'albums' })
  findAll(
    @GqlHeader(FAMILY_HEADER, GetFamilyPipe) family: Family,
  ): Promise<Album[]> {
    return this.albumService.findAll(family.id);
  }

  @Query(() => Album, { name: 'album' })
  findOne(@Args('id', { type: () => String }) id: string): Promise<Album> {
    return this.albumService.findOne(id);
  }

  @Mutation(() => Album)
  updateAlbum(@Args('input') input: UpdateAlbumInput): Promise<Album> {
    return this.albumService.update(input.id, input);
  }

  @Mutation(() => Boolean)
  removeAlbum(
    @Args('id', { type: () => String }) id: string,
  ): Promise<boolean> {
    return this.albumService.remove(id).then(() => true);
  }

  @ResolveField(() => [Media])
  medias(
    @Parent() album: Album,
    @Args('offset', { type: () => Int, nullable: true }) offset = 0,
    @Args('size', { type: () => Int, nullable: true }) size = 200,
  ): Promise<Media[]> {
    return this.queryBus.execute<GetAlbumMediasQuery, Media[]>(
      new GetAlbumMediasQuery(album.id, offset, size, album.familyId),
    );
  }

  @ResolveField(() => Account)
  createdBy(@Parent() album: Album): Promise<Account> {
    return this.queryBus.execute<GetAccountQuery, Account>(
      new GetAccountQuery(album.creatorId),
    );
  }

  @ResolveField(() => [AlbumFavorite])
  favorites(
    @Parent() album: Album,
    @Args('offset', { type: () => Int, nullable: true }) offset = 0,
    @Args('size', { type: () => Int, nullable: true }) size = 200,
  ): Promise<AlbumFavorite[]> {
    return this.queryBus.execute<GetAlbumFavoritesQuery, AlbumFavorite[]>(
      new GetAlbumFavoritesQuery(album.id, offset, size),
    );
  }
}
