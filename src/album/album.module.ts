import { Module } from '@nestjs/common';
import { AlbumService } from './album.service';
import { AlbumResolver } from './album.resolver';
import { MongooseModule } from '@nestjs/mongoose';
import { AlbumDb, AlbumSchema } from './database/album.db';
import { GetAlbumHandler } from './handler/get-album.handler';
import { CqrsModule } from '@nestjs/cqrs';
import { PubSubModule } from '@pub-sub/pub-sub.module';
import { GetAlbumsHandler } from '@album/handler/get-albums.handler';

@Module({
  providers: [AlbumResolver, AlbumService, GetAlbumHandler, GetAlbumsHandler],
  imports: [
    CqrsModule,
    PubSubModule,
    MongooseModule.forFeature([
      {
        name: AlbumDb.name,
        schema: AlbumSchema,
      },
    ]),
  ],
})
export class AlbumModule {}
