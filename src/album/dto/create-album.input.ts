import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class CreateAlbumInput {
  @Field(() => String)
  creatorId: string;

  @Field(() => String, { nullable: true })
  title: string;

  @Field(() => String, { nullable: true })
  description: string;

  @Field(() => [String], { nullable: true })
  tags: string[];

  familyId: string;
}
