import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class UpdateAlbumInput {
  @Field(() => String)
  id: string;

  @Field(() => String, { nullable: true })
  title: string;

  @Field(() => String, { nullable: true })
  description: string;

  @Field(() => [String], { nullable: true })
  tags: string[];
}
