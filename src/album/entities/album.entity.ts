import { Field, ID, ObjectType } from '@nestjs/graphql';
import { AlbumDocument } from '../database/album.db';
import { ApiProperty } from '@nestjs/swagger';

@ObjectType()
export class Album {
  @ApiProperty()
  @Field()
  createdAt: number;

  @ApiProperty()
  @Field()
  updatedAt: number;

  @Field(() => ID)
  id: string;

  creatorId: string;

  @Field(() => String, { nullable: true })
  title: string;

  @Field(() => String, { nullable: true })
  description: string;

  @Field(() => [String], { nullable: true })
  tags: string[];

  familyId: string;

  static map(model: AlbumDocument) {
    const alb = new Album();
    alb.id = model.id;
    alb.creatorId = model.creatorId;
    alb.title = model.title;
    alb.description = model.description;
    alb.tags = model.tags;
    alb.familyId = model.familyId;

    alb.createdAt = model.createdAt;
    alb.updatedAt = model.updatedAt;
    return alb;
  }
}
