import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { CreateAlbumInput } from '../dto/create-album.input';

export type AlbumDocument = AlbumDb & Document;

@Schema({
  collection: 'album',
  timestamps: true,
})
export class AlbumDb {
  @Prop()
  createdAt: number;

  @Prop()
  updatedAt: number;

  @Prop()
  creatorId: string;

  @Prop()
  title: string;

  @Prop()
  description: string;

  @Prop()
  familyId: string;

  @Prop([String])
  tags: string[];

  static mapInput(input: CreateAlbumInput): AlbumDb {
    const model = new AlbumDb();
    model.creatorId = input.creatorId;
    model.title = input.title;
    model.description = input.description;
    model.tags = input.tags;
    model.familyId = input.familyId;
    return model;
  }
}

export const AlbumSchema = SchemaFactory.createForClass(AlbumDb);
