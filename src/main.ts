import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Logger, ValidationPipe } from '@nestjs/common';
import * as compression from 'compression';
import * as cors from 'cors';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AuthModule } from '@auth/auth.module';

async function bootstrap(
  port = process.env.PORT ? Number(process.env.PORT) : 3000,
) {
  const startTime = new Date().getTime();

  const app = await NestFactory.create(AppModule);

  const options = new DocumentBuilder()
    .setTitle('Auth API')
    .setDescription('The Auth API description')
    .setVersion('1.0.0')
    .addBasicAuth()
    .build();
  const document = SwaggerModule.createDocument(app, options, {
    include: [AuthModule],
  });
  SwaggerModule.setup('api/auth', app, document);

  app.useGlobalPipes(new ValidationPipe());
  app.use(compression());
  app.use(cors());

  await app.listen(port, () => {
    const now = new Date().getTime();
    Logger.log(`Started in ${now - startTime}ms`, 'App');
  });
}

bootstrap();
