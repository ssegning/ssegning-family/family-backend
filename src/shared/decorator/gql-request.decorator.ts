import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { IncomingMessage } from 'http';
import { extractAppAccountIdFromKeycloak } from '@shared/util/keycloak-spi-utils';

export const GqlRequest = createParamDecorator(
  (data: string, ctx: ExecutionContext) => {
    const request: IncomingMessage = ctx.getArgByIndex(2).req;
    return data ? request[data.toLowerCase()] : request;
  },
);

export const GqlAccountId = createParamDecorator(
  (data: string, ctx: ExecutionContext): string => {
    const request: IncomingMessage & { user: any } = ctx.getArgByIndex(2).req;
    const sub = request.user.sub;
    return extractAppAccountIdFromKeycloak(sub);
  },
);
