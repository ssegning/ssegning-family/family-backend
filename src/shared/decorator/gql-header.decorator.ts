import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { IncomingMessage } from 'http';

export const GqlHeader = createParamDecorator(
  (data: string, ctx: ExecutionContext) => {
    const request: IncomingMessage = ctx.getArgByIndex(2).req;
    const headers = request.headers;
    return data ? headers[data.toLowerCase()] : headers;
  },
);
