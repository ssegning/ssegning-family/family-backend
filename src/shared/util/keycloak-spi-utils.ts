export function extractAppAccountIdFromKeycloak(sub: string): string {
  const str = sub as string;
  const lastIndexOfTwoPoints = str.lastIndexOf(':');
  return str.substring(lastIndexOfTwoPoints + 1);
}
