import { GetFamilyMemberPipe } from './get-family-member.pipe';

describe('GetFamilyMemberPipe', () => {
  it('should be defined', () => {
    expect(new GetFamilyMemberPipe()).toBeDefined();
  });
});
