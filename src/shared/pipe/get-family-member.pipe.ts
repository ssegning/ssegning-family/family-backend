import { ArgumentMetadata, Injectable, PipeTransform } from '@nestjs/common';
import { GetFamilyMemberQuery } from '@family-member/model/get-family-member.query';
import { GetFamilyMemberResponse } from '@family-member/model/get-family-member.response';
import { FamilyMember } from '@family-member/entities/family-member.entity';
import { QueryBus } from '@nestjs/cqrs';

@Injectable()
export class GetFamilyMemberPipe
  implements PipeTransform<string, Promise<FamilyMember>> {
  constructor(private readonly queryBus: QueryBus<GetFamilyMemberQuery>) {}

  async transform(
    value: string,
    metadata: ArgumentMetadata,
  ): Promise<FamilyMember> {
    const { member } = await this.queryBus.execute<
      GetFamilyMemberQuery,
      GetFamilyMemberResponse
    >(new GetFamilyMemberQuery(value));
    return member;
  }
}
