import { ArgumentMetadata, Injectable, PipeTransform } from '@nestjs/common';
import { Account } from '@account/entities/account.entity';
import { QueryBus } from '@nestjs/cqrs';
import { GetAccountQuery } from '@account/model/get-account.query';
import { isEmail } from 'class-validator';

@Injectable()
export class GetAccountPipe implements PipeTransform<string, Promise<Account>> {
  constructor(private readonly queryBus: QueryBus) {
  }

  async transform(identifier: string, _: ArgumentMetadata): Promise<Account> {
    const byEmail = isEmail(identifier);
    return this.queryBus.execute<GetAccountQuery, Account>(
      new GetAccountQuery(identifier, byEmail),
    );
  }
}
