import {
  ArgumentMetadata,
  BadRequestException,
  Injectable,
  PipeTransform,
} from '@nestjs/common';
import { QueryBus } from '@nestjs/cqrs';
import { GetFamilyQuery } from '@family/model/get-family.query';
import { isMongoId, isString } from 'class-validator';
import { GetFamilyResponse } from '@family/model/get-family.response';
import { Family } from '@family/entities/family.entity';

@Injectable()
export class GetFamilyPipe implements PipeTransform<string, Promise<Family>> {
  constructor(private readonly queryBus: QueryBus<GetFamilyQuery>) {}

  async transform(value: string, _: ArgumentMetadata): Promise<Family> {
    if (!isString(value)) {
      throw new BadRequestException('family.find.error');
    }
    const byMongoId = isMongoId(value);
    const { family } = await this.queryBus.execute<
      GetFamilyQuery,
      GetFamilyResponse
    >(new GetFamilyQuery(value, byMongoId));

    return family;
  }
}
