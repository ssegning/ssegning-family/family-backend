export class UploadResponse {
  constructor(
    public readonly url: string,
    public readonly width: number,
    public readonly height: number,
    public readonly mimeType: string,
  ) {}
}
