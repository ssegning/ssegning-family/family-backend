import {
  CallHandler,
  ExecutionContext,
  mixin,
  NestInterceptor,
  Optional,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { FileUpload } from 'graphql-upload';
import { GqlExecutionContext } from '@nestjs/graphql';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { UploadFileCommand } from '@document/model/upload-file.command';
import { UploadFileResponse } from '@document/model/upload-file.response';
import { UploadResponse } from '@shared/model/upload.response';
import { GetDimensionResponse } from '@document/model/get-dimension.response';
import { GetDimensionQuery } from '@document/model/get-dimension.query';

export interface FieldDescription {
  name: string;
}

export type IField = string | FieldDescription;

export function GraphqlFileFieldsInterceptor(
  uploadFields: IField[],
): NestInterceptor {
  class UploadInterceptor implements NestInterceptor {
    constructor(
      @Optional() private commandBus: CommandBus<UploadFileCommand>,
      @Optional() private queryBus: QueryBus<GetDimensionQuery>,
    ) {}

    async intercept(
      context: ExecutionContext,
      next: CallHandler,
    ): Promise<Observable<any>> {
      const ctx = GqlExecutionContext.create(context);
      const args = ctx.getArgs();

      const fileUploads: Promise<void>[] = uploadFields.map(
        async (fieldName) => {
          const name =
            typeof fieldName === 'string' ? fieldName : fieldName.name;
          const file: FileUpload = await args[name];
          if (file) {
            const {
              fileUrl,
            }: UploadFileResponse = await this.commandBus.execute(
              new UploadFileCommand(
                file.filename,
                file.mimetype,
                file.createReadStream(),
              ),
            );
            const dimensionResponse: GetDimensionResponse = await this.queryBus
              .execute(new GetDimensionQuery(file.createReadStream()))
              .catch((err) => {
                console.error(err);
                return {};
              });
            args[name] = new UploadResponse(
              fileUrl,
              dimensionResponse.width,
              dimensionResponse.height,
              dimensionResponse.mimeType,
            );
          } else {
            args[name] = undefined;
          }
        },
      );

      await Promise.all(fileUploads);

      return next.handle();
    }
  }

  return mixin(UploadInterceptor) as any;
}
