import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class CreateMediaInput {
  @Field(() => [String])
  referencedMembers: string[];

  url: string;
  width: number;
  height: number;
  mimeType: string;
  familyId: string;

  @Field(() => String, { nullable: true })
  description: string;

  @Field(() => [String], { nullable: false })
  albumIds: string[];

  @Field(() => [String])
  tags: string[];
}
