import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class UpdateMediaInput {
  @Field(() => [String])
  referencedMembers: string[];

  @Field(() => String)
  id: string;

  @Field(() => String, { nullable: true })
  description: string;

  @Field(() => [String], { nullable: false })
  albumIds: string[];

  @Field(() => [String])
  tags: string[];
}
