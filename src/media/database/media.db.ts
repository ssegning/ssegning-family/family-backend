import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type MediaDocument = MediaDb & Document;

@Schema({
  collection: 'media',
  timestamps: true,
})
export class MediaDb {
  @Prop()
  createdAt: number;

  @Prop()
  updatedAt: number;

  @Prop([String])
  referencedMembers: string[];

  @Prop()
  metaData: Map<string, string>;

  @Prop({ required: true })
  url: string;

  @Prop()
  width: number;

  @Prop()
  height: number;

  @Prop()
  mimeType: string;

  @Prop({ required: true })
  familyId: string;

  @Prop()
  description: string;

  @Prop([String])
  albumIds: string[];

  @Prop([String])
  tags: string[];
}

export const MediaSchema = SchemaFactory.createForClass(MediaDb);
