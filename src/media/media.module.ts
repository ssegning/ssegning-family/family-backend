import { Module } from '@nestjs/common';
import { MediaService } from './services/media.service';
import { MediaResolver } from './media.resolver';
import { GetAlbumMediasHandler } from './handler/get-album-medias.handler';
import { MongooseModule } from '@nestjs/mongoose';
import { MediaDb, MediaSchema } from './database/media.db';
import { GetMediaHandler } from './handler/get-media.handler';
import { CqrsModule } from '@nestjs/cqrs';
import { PubSubModule } from '@pub-sub/pub-sub.module';

@Module({
  providers: [
    MediaResolver,
    MediaService,
    GetAlbumMediasHandler,
    GetMediaHandler,
  ],
  imports: [
    CqrsModule,
    PubSubModule,
    MongooseModule.forFeature([
      {
        name: MediaDb.name,
        schema: MediaSchema,
      },
    ]),
  ],
})
export class MediaModule {
}
