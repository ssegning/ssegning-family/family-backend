import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateMediaInput } from '../dto/create-media.input';
import { UpdateMediaInput } from '../dto/update-media.input';
import { Media } from '../entities/media.entity';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { MediaDb, MediaDocument } from '../database/media.db';

@Injectable()
export class MediaService {
  constructor(
    @InjectModel(MediaDb.name)
    private readonly mediaModel: Model<MediaDocument>,
  ) {}

  async create(input: CreateMediaInput): Promise<Media> {
    const dto = new MediaDb();
    dto.referencedMembers = input.referencedMembers;
    dto.url = input.url;
    dto.description = input.description;
    dto.albumIds = input.albumIds;
    dto.tags = input.tags;

    dto.width = input.width;
    dto.height = input.height;
    dto.mimeType = input.mimeType;
    dto.familyId = input.familyId;

    const model = new this.mediaModel(dto);
    const createdModel = await model.save();
    return Media.map(createdModel);
  }

  async findAll(familyId: string) {
    const models = await this.mediaModel.find({ familyId }).exec();
    return models.map(Media.map);
  }

  async findOne(id: string) {
    const model = await this.findById(id);
    return Media.map(model);
  }

  async update(id: string, input: UpdateMediaInput) {
    const model = await this.findById(id);

    model.referencedMembers =
      input.referencedMembers || model.referencedMembers;
    model.description = input.description || model.description;
    model.albumIds = input.albumIds || model.albumIds;
    model.tags = input.tags || model.tags;

    const newModel = await model.save();
    return Media.map(newModel);
  }

  async remove(id: string): Promise<void> {
    const model = await this.findById(id);
    await model.deleteOne();
  }

  async findForAlbum(
    albumId: string,
    offset: number,
    size: number,
    familyId: string,
  ): Promise<Media[]> {
    const list = await this.mediaModel
      .find({ albumId })
      .skip(offset)
      .limit(size)
      .exec();
    return list.map(Media.map);
  }

  private async findById(id: string) {
    const model = await this.mediaModel.findById(id).exec();
    if (!model) {
      throw new NotFoundException('media.not.found');
    }
    return model;
  }
}
