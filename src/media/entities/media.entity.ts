import { Field, ID, ObjectType } from '@nestjs/graphql';
import { MediaDocument } from '../database/media.db';
import { ApiProperty } from '@nestjs/swagger';

@ObjectType()
export class Media {
  @ApiProperty()
  @Field()
  createdAt: number;

  @ApiProperty()
  @Field()
  updatedAt: number;

  @Field(() => ID)
  id: string;

  referencedMembers: string[];

  url: string;

  @ApiProperty()
  @Field()
  width: number;

  @ApiProperty()
  @Field()
  height: number;

  @ApiProperty()
  @Field()
  mimeType: string;

  @ApiProperty()
  familyId: string;

  @Field(() => String)
  description: string;

  albumIds: string[];

  @Field(() => [String])
  tags: string[];

  static map(doc: MediaDocument): Media {
    const media = new Media();
    media.id = doc.id;
    media.referencedMembers = doc.referencedMembers;
    media.url = doc.url;
    media.description = doc.description;
    media.albumIds = doc.albumIds;
    media.tags = doc.tags;

    media.createdAt = doc.createdAt;
    media.updatedAt = doc.updatedAt;

    media.width = doc.width;
    media.height = doc.height;
    media.mimeType = doc.mimeType;

    media.familyId = doc.familyId;
    return media;
  }
}
