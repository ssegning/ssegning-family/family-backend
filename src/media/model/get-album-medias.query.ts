export class GetAlbumMediasQuery {
  constructor(
    public readonly albumId: string,
    public readonly offset: number,
    public readonly size: number,
    public readonly familyId: string,
  ) {}
}
