import {
  Args,
  Int,
  Mutation,
  Parent,
  Query,
  ResolveField,
  Resolver,
} from '@nestjs/graphql';
import { MediaService } from './services/media.service';
import { Media } from './entities/media.entity';
import { CreateMediaInput } from './dto/create-media.input';
import { UpdateMediaInput } from './dto/update-media.input';
import { QueryBus } from '@nestjs/cqrs';
import { GetMediaFavoritesQuery } from '@media-favorite/model/get-media-favorites.query';
import { GetMediaFavoriteCountQuery } from '@media-favorite/model/get-media-favorite-count.query';
import { Album } from '@album/entities/album.entity';
import { MediaFavorite } from '@media-favorite/entities/media-favorite.entity';
import { GraphQLUpload } from 'apollo-server-express';
import { GetAlbumsQuery } from '@album/model/get-albums.query';
import { GraphqlFileFieldsInterceptor } from '@shared/interceptor/upload.interceptor';
import { UseInterceptors } from '@nestjs/common';
import { SignUrlQuery } from '@document/model/sign-url.query';
import { SignUrlResponse } from '@document/model/sign-url.response';
import { UploadResponse } from '@shared/model/upload.response';
import { Family } from '@family/entities/family.entity';
import { FAMILY_HEADER } from '@shared/constants/family-header-name';
import { GqlHeader } from '@shared/decorator/gql-header.decorator';
import { GetFamilyPipe } from '@shared/pipe/get-family.pipe';
import { FamilyMember } from '@family-member/entities/family-member.entity';
import { GetFamilyMembersResponse } from '@family-member/model/get-family-members.response';
import { GetFamilyMembersQuery } from '@family-member/model/get-family-members.query';

@Resolver(() => Media)
export class MediaResolver {
  constructor(
    private readonly mediaService: MediaService,
    private readonly queryBus: QueryBus,
  ) {}

  @UseInterceptors(GraphqlFileFieldsInterceptor(['file']))
  @Mutation(() => Media)
  async createMedia(
    @Args('input') input: CreateMediaInput,
    @Args({ name: 'file', type: () => GraphQLUpload })
    uploadResponse: UploadResponse,
    @GqlHeader(FAMILY_HEADER, GetFamilyPipe) family: Family,
  ): Promise<Media> {
    input.url = uploadResponse.url;
    input.width = uploadResponse.width;
    input.height = uploadResponse.height;
    input.mimeType = uploadResponse.mimeType;
    input.familyId = family.id;
    return this.mediaService.create(input);
  }

  @Query(() => [Media], { name: 'medias' })
  findAll(
    @GqlHeader(FAMILY_HEADER, GetFamilyPipe) family: Family,
  ): Promise<Media[]> {
    return this.mediaService.findAll(family.id);
  }

  @Query(() => Media, { name: 'media' })
  findOne(@Args('id', { type: () => String }) id: string): Promise<Media> {
    return this.mediaService.findOne(id);
  }

  @Mutation(() => Media)
  async updateMedia(@Args('input') input: UpdateMediaInput): Promise<Media> {
    return this.mediaService.update(input.id, input);
  }

  @Mutation(() => Boolean)
  removeMedia(
    @Args('id', { type: () => String }) id: string,
  ): Promise<boolean> {
    return this.mediaService.remove(id).then(() => true);
  }

  @ResolveField(() => [FamilyMember])
  async references(
    @Parent() media: Media,
    @Args('offset', { type: () => Int, nullable: true }) offset = 0,
    @Args('size', { type: () => Int, nullable: true }) size = 200,
  ): Promise<FamilyMember[]> {
    const { members }: GetFamilyMembersResponse = await this.queryBus.execute<
      GetFamilyMembersQuery,
      GetFamilyMembersResponse
    >(new GetFamilyMembersQuery(media.referencedMembers, offset, size));
    return members;
  }

  @ResolveField(() => [MediaFavorite])
  favorites(
    @Parent() media: Media,
    @Args('offset', { type: () => Int, nullable: true }) offset = 0,
    @Args('size', { type: () => Int, nullable: true }) size = 200,
  ): Promise<MediaFavorite[]> {
    return this.queryBus.execute<GetMediaFavoritesQuery, MediaFavorite[]>(
      new GetMediaFavoritesQuery(media.id, offset, size),
    );
  }

  @ResolveField(() => Int)
  favoritesCounts(@Parent() media: Media): Promise<number> {
    return this.queryBus.execute<GetMediaFavoriteCountQuery, number>(
      new GetMediaFavoriteCountQuery(media.id),
    );
  }

  @ResolveField(() => [Album], { nullable: true })
  albums(
    @Parent() media: Media,
    @Args('offset', { type: () => Int, nullable: true }) offset = 0,
    @Args('size', { type: () => Int, nullable: true }) size = 200,
  ): Promise<Album[]> {
    return this.queryBus.execute<GetAlbumsQuery, Album[]>(
      new GetAlbumsQuery(media.albumIds, offset, size),
    );
  }

  @ResolveField(() => String)
  async url(
    @Parent() media: Media,
    @Args('width', { type: () => Int, nullable: true }) width: number,
    @Args('height', { type: () => Int, nullable: true }) height: number,
  ): Promise<string> {
    let nextUrl = media.url;
    if (width) {
      const newWidth = Math.floor(width);
      let nextHeight = height;
      if (!nextHeight) {
        nextHeight = Math.floor((media.height * newWidth) / media.width);
      }
      const response = await this.queryBus.execute<
        SignUrlQuery,
        SignUrlResponse
      >(new SignUrlQuery(media.url, newWidth, nextHeight));
      nextUrl = response.url;
    }
    return nextUrl;
  }
}
