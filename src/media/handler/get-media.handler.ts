import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { Media } from '../entities/media.entity';
import { MediaService } from '../services/media.service';
import { GetMediaQuery } from '../model/get-media.query';

@QueryHandler(GetMediaQuery)
export class GetMediaHandler implements IQueryHandler<GetMediaQuery, Media> {
  constructor(private readonly mediaService: MediaService) {}

  async execute(query: GetMediaQuery): Promise<Media> {
    return this.mediaService.findOne(query.id);
  }
}
