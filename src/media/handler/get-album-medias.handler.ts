import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { GetAlbumMediasQuery } from '../model/get-album-medias.query';
import { Media } from '../entities/media.entity';
import { MediaService } from '../services/media.service';

@QueryHandler(GetAlbumMediasQuery)
export class GetAlbumMediasHandler
  implements IQueryHandler<GetAlbumMediasQuery, Media[]> {
  constructor(private readonly mediaService: MediaService) {}

  async execute(query: GetAlbumMediasQuery): Promise<Media[]> {
    return this.mediaService.findForAlbum(
      query.albumId,
      query.offset,
      query.size,
      query.familyId,
    );
  }
}
