import { Module } from '@nestjs/common';
import { GQLService } from './service/g-q-l/g-q-l.service';
import { KeycloakConnectModule } from 'nest-keycloak-connect';
import { ConfigModule, ConfigService } from '@nestjs/config';

@Module({
  providers: [GQLService],
  exports: [GQLService, KeycloakConnectModule],
  imports: [
    KeycloakConnectModule.registerAsync({
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        authServerUrl: configService.get('KEYCLOAK_FRONTEND_URL'),
        realm: configService.get('KEYCLOAK_REALM'),
        clientId: configService.get('KEYCLOAK_CLIENT_ID'),
        secret: configService.get('KEYCLOAK_SECRET'),
      }),
      imports: [ConfigModule],
    }),
  ],
})
export class KeycloakModule {}
