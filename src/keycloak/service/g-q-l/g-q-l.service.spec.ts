import { Test, TestingModule } from '@nestjs/testing';
import { GQLService } from './g-q-l.service';

describe('GQLService', () => {
  let service: GQLService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [GQLService],
    }).compile();

    service = module.get<GQLService>(GQLService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
