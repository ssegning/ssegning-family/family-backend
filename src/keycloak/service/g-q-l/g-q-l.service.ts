import { Inject, Injectable, UnauthorizedException } from '@nestjs/common';
import { KEYCLOAK_INSTANCE } from 'nest-keycloak-connect';
import * as KeycloakConnect from 'keycloak-connect';

@Injectable()
export class GQLService {
  constructor(
    @Inject(KEYCLOAK_INSTANCE)
    private keycloak: KeycloakConnect.Keycloak,
  ) {}

  async validateToken(jwt: string): Promise<any> {
    try {
      const result = await this.keycloak.grantManager.validateAccessToken(jwt);
      if (typeof result === 'string') {
        return await this.keycloak.grantManager.userInfo(jwt);
      }
    } catch (ex) {
      console.error(ex);
    }
    throw new UnauthorizedException('cannot.validate.token');
  }
}
