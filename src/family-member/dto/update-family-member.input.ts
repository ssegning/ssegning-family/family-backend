import { Field, InputType, Int, PartialType } from '@nestjs/graphql';
import { CreateFamilyMemberInput } from '@family-member/dto/create-family-member.input';

@InputType()
export class UpdateFamilyMemberInput extends PartialType(
  CreateFamilyMemberInput,
) {
  @Field(() => Int)
  id: string;
}
