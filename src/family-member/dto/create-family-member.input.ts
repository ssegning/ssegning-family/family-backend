import { Field, InputType } from '@nestjs/graphql';
import { MemberRole } from '@family-member/database/family-member.db';

@InputType()
export class CreateFamilyMemberInput {
  @Field(() => MemberRole, { defaultValue: 'member', nullable: true })
  role: MemberRole = MemberRole.MEMBER;
}
