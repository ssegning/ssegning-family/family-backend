import { Module } from '@nestjs/common';
import { FamilyMemberService } from './family-member.service';
import { FamilyMemberResolver } from './family-member.resolver';
import { CqrsModule } from '@nestjs/cqrs';
import { GetFamilyMemberHandler } from './handler/get-family-member.handler';
import { PubSubModule } from '@pub-sub/pub-sub.module';
import { MongooseModule } from '@nestjs/mongoose';
import {
  FamilyMemberDb,
  FamilyMemberSchema,
} from '@family-member/database/family-member.db';
import { GetFamilyMembersService } from './handler/get-family-members.service';
import { CreateFamilyMemberHandler } from './handler/create-family-member.handler';
import { MemberSaga } from './handler/member.saga';
import { PublishFamilyMemberHandler } from './handler/publish-family-member.handler';

@Module({
  providers: [
    FamilyMemberResolver,
    FamilyMemberService,
    GetFamilyMemberHandler,
    GetFamilyMembersService,
    CreateFamilyMemberHandler,
    MemberSaga,
    PublishFamilyMemberHandler,
  ],
  imports: [
    PubSubModule,
    CqrsModule,
    MongooseModule.forFeature([
      {
        name: FamilyMemberDb.name,
        schema: FamilyMemberSchema,
      },
    ]),
  ],
})
export class FamilyMemberModule {}
