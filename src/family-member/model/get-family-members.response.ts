import { FamilyMember } from '../entities/family-member.entity';

export class GetFamilyMembersResponse {
  constructor(public readonly members: FamilyMember[]) {}
}
