import { IQuery } from '@nestjs/cqrs';

export class GetFamilyMemberQuery implements IQuery {
  constructor(public readonly id: string) {}
}
