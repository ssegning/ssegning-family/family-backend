import { ICommand } from '@nestjs/cqrs';
import { FamilyMember } from '@family-member/entities/family-member.entity';

export class PublishFamilyMemberCommand implements ICommand {
  constructor(public readonly member: FamilyMember) {}
}
