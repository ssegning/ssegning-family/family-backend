import { IQuery } from '@nestjs/cqrs';

export class GetFamilyMembersQuery implements IQuery {
  constructor(
    public readonly ids: string[],
    public readonly offset: number = 0,
    public readonly size: number = 200,
  ) {}
}
