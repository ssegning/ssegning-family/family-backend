import { FamilyMember } from '../entities/family-member.entity';

export class GetFamilyMemberResponse {
  constructor(public readonly member: FamilyMember) {}
}
