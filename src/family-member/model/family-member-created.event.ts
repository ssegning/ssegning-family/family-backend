import { IEvent } from '@nestjs/cqrs';
import { FamilyMember } from '@family-member/entities/family-member.entity';

export class FamilyMemberCreatedEvent implements IEvent {
  constructor(public readonly member: FamilyMember) {}
}
