import { FamilyMember } from '@family-member/entities/family-member.entity';

export class CreateFamilyMemberResponse {
  constructor(public readonly member: FamilyMember) {}
}
