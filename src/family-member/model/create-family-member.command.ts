import { ICommand } from '@nestjs/cqrs';
import { Account } from '@account/entities/account.entity';
import { CreateFamilyMemberInput } from '@family-member/dto/create-family-member.input';
import { Family } from '@family/entities/family.entity';

export class CreateFamilyMemberCommand implements ICommand {
  constructor(
    public readonly account: Account,
    public readonly input: CreateFamilyMemberInput,
    public readonly family: Family,
  ) {}
}
