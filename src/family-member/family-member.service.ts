import { Injectable, NotFoundException } from '@nestjs/common';
import { FamilyMember } from '@family-member/entities/family-member.entity';
import { InjectModel } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { Model } from 'mongoose';
import {
  FamilyMemberDb,
  FamilyMemberDocument,
} from '@family-member/database/family-member.db';

@Injectable()
export class FamilyMemberService {
  constructor(
    @InjectModel(FamilyMemberDb.name)
    private readonly memberModel: Model<FamilyMemberDocument>,
  ) {}

  async create(member: FamilyMember): Promise<FamilyMember> {
    const previous = await this.findPrevious(member.accountId, member.familyId);
    if (previous) {
      return FamilyMember.map(previous);
    }
    const model = new this.memberModel(member);
    const createdModel = await model.save();
    return FamilyMember.map(createdModel);
  }

  async findAll(
    familyId: string,
    offset = 0,
    limit = 200,
  ): Promise<FamilyMember[]> {
    const models = await this.memberModel
      .find({ familyId })
      .skip(offset)
      .limit(limit)
      .exec();
    return models.map(FamilyMember.map);
  }

  async findAllByAccountId(
    accountId: string,
    offset = 0,
    limit = 200,
  ): Promise<FamilyMember[]> {
    const models = await this.memberModel
      .find({ accountId })
      .skip(offset)
      .limit(limit)
      .exec();
    return models.map(FamilyMember.map);
  }

  async findOne(id: string): Promise<FamilyMember> {
    const found = await this.findById(id);
    return FamilyMember.map(found);
  }

  async update(id: string, member: FamilyMember): Promise<FamilyMember> {
    const model = await this.findById(id);
    model.role = member.role || model.role;

    await model.save();
    return FamilyMember.map(model);
  }

  async remove(id: string): Promise<void> {
    const model = await this.findById(id);
    await model.deleteOne();
  }

  async findByIds(
    ids: string[],
    offset: number,
    size: number,
  ): Promise<FamilyMember[]> {
    const list = await this.memberModel
      .find()
      .where('_id')
      .in(ids.map((id) => mongoose.Types.ObjectId(id)))
      .skip(offset)
      .limit(size)
      .exec();
    return list.map(FamilyMember.map);
  }

  private async findById(id: string): Promise<FamilyMemberDocument> {
    const model = await this.memberModel.findById(id).exec();
    if (!model) {
      throw new NotFoundException('family-member.not.found');
    }
    return model;
  }

  private async findPrevious(
    accountId: string,
    familyId: string,
  ): Promise<FamilyMemberDocument> {
    return await this.memberModel.findOne({ accountId, familyId }).exec();
  }
}
