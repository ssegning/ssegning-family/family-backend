import { Field, ID, ObjectType } from '@nestjs/graphql';
import {
  FamilyMemberDocument,
  MemberRole,
} from '@family-member/database/family-member.db';

@ObjectType()
export class FamilyMember {
  static MEMBER_SUB = 'member_sub';

  @Field(() => ID)
  id: string;

  @Field()
  createdAt: number;

  @Field()
  updatedAt: number;

  accountId: string;

  familyId: string;

  @Field(() => MemberRole)
  role: MemberRole;

  static map(doc: FamilyMemberDocument): FamilyMember {
    const family = new FamilyMember();
    family.id = doc.id;
    family.createdAt = doc.createdAt;
    family.updatedAt = doc.updatedAt;
    family.accountId = doc.accountId;
    family.familyId = doc.familyId;
    family.role = doc.role;
    return family;
  }
}
