import { Test, TestingModule } from '@nestjs/testing';
import { FamilyMemberResolver } from './family-member.resolver';
import { FamilyMemberService } from './family-member.service';

describe('FamilyMemberResolver', () => {
  let resolver: FamilyMemberResolver;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FamilyMemberResolver, FamilyMemberService],
    }).compile();

    resolver = module.get<FamilyMemberResolver>(FamilyMemberResolver);
  });

  it('should be defined', () => {
    expect(resolver).toBeDefined();
  });
});
