import {
  Args,
  ID,
  Int,
  Mutation,
  Parent,
  Query,
  ResolveField,
  Resolver,
  Subscription,
} from '@nestjs/graphql';
import { FamilyMemberService } from './family-member.service';
import { FamilyMember } from './entities/family-member.entity';
import { CreateFamilyMemberInput } from './dto/create-family-member.input';
import { UpdateFamilyMemberInput } from './dto/update-family-member.input';
import { GqlHeader } from '@shared/decorator/gql-header.decorator';
import { FAMILY_HEADER } from '@shared/constants/family-header-name';
import { GetFamilyPipe } from '@shared/pipe/get-family.pipe';
import { Family } from '@family/entities/family.entity';
import { GetAccountPipe } from '@shared/pipe/get-account.pipe';
import { Account } from '@account/entities/account.entity';
import { GetFamilyQuery } from '@family/model/get-family.query';
import { GetFamilyResponse } from '@family/model/get-family.response';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { GetAccountQuery } from '@account/model/get-account.query';
import { CreateFamilyMemberCommand } from '@family-member/model/create-family-member.command';
import { CreateFamilyMemberResponse } from '@family-member/model/create-family-member.response';
import { RedisPubSub } from 'graphql-redis-subscriptions';
import { Public } from 'nest-keycloak-connect';
import { GqlAccountId } from '@shared/decorator/gql-request.decorator';
import { extractAppAccountIdFromKeycloak } from '@shared/util/keycloak-spi-utils';

@Resolver(() => FamilyMember)
export class FamilyMemberResolver {
  constructor(
    private readonly familyMemberService: FamilyMemberService,
    private readonly queryBus: QueryBus,
    private readonly commandBus: CommandBus,
    private readonly pubSub: RedisPubSub,
  ) {}

  @Mutation(() => FamilyMember)
  async createFamilyMember(
    @Args('accountId', { type: () => ID }, GetAccountPipe) account: Account,
    @Args('input')
    input: CreateFamilyMemberInput,
    @GqlHeader(FAMILY_HEADER, GetFamilyPipe) family: Family,
  ) {
    const command = new CreateFamilyMemberCommand(account, input, family);
    const response: CreateFamilyMemberResponse = await this.commandBus.execute(
      command,
    );
    return response.member;
  }

  @Query(() => [FamilyMember], { name: 'familyMembers' })
  findAll(@GqlHeader(FAMILY_HEADER, GetFamilyPipe) family: Family) {
    return this.familyMemberService.findAll(family.id);
  }

  @Query(() => [FamilyMember], { name: 'familyMembersByAccountId' })
  findAllByAccountId(@GqlAccountId(GetAccountPipe) account: Account) {
    return this.familyMemberService.findAllByAccountId(account.id);
  }

  @Query(() => FamilyMember, { name: 'familyMember' })
  findOne(@Args('id', { type: () => ID }) id: string) {
    return this.familyMemberService.findOne(id);
  }

  @Mutation(() => FamilyMember)
  updateFamilyMember(@Args('input') input: UpdateFamilyMemberInput) {
    const member = new FamilyMember();
    member.role = input.role;
    return this.familyMemberService.update(input.id, member);
  }

  @Mutation(() => FamilyMember)
  removeFamilyMember(@Args('id', { type: () => Int }) id: string) {
    return this.familyMemberService.remove(id);
  }

  @ResolveField(() => Family)
  async family(@Parent() member: FamilyMember): Promise<Family> {
    const { family } = await this.queryBus.execute<
      GetFamilyQuery,
      GetFamilyResponse
    >(new GetFamilyQuery(member.familyId, true));
    return family;
  }

  @ResolveField(() => Account)
  account(@Parent() member: FamilyMember): Promise<Account> {
    return this.queryBus.execute<GetAccountQuery, Account>(
      new GetAccountQuery(member.accountId),
    );
  }

  @Public()
  @Subscription(() => FamilyMember, {
    resolve: (value) => value,
    filter: (payload: FamilyMember, _, ctx) => {
      return (
        payload.accountId === extractAppAccountIdFromKeycloak(ctx.req.user.sub)
      );
    },
  })
  accountMembershipUpdates(): AsyncIterator<FamilyMember> {
    return this.pubSub.asyncIterator(FamilyMember.MEMBER_SUB);
  }
}
