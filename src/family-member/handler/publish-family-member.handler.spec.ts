import { Test, TestingModule } from '@nestjs/testing';
import { PublishFamilyMemberHandler } from './publish-family-member.handler';

describe('PublishFamilyMemberService', () => {
  let service: PublishFamilyMemberHandler;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PublishFamilyMemberHandler],
    }).compile();

    service = module.get<PublishFamilyMemberHandler>(
      PublishFamilyMemberHandler,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
