import { Test, TestingModule } from '@nestjs/testing';
import { GetFamilyMembersService } from './get-family-members.service';

describe('GetFamilyMembersService', () => {
  let service: GetFamilyMembersService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [GetFamilyMembersService],
    }).compile();

    service = module.get<GetFamilyMembersService>(GetFamilyMembersService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
