import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { GetFamilyMemberQuery } from '@family-member/model/get-family-member.query';
import { GetFamilyMemberResponse } from '@family-member/model/get-family-member.response';
import { FamilyMemberService } from '@family-member/family-member.service';
import { FamilyMember } from '@family-member/entities/family-member.entity';

@QueryHandler(GetFamilyMemberQuery)
export class GetFamilyMemberHandler
  implements IQueryHandler<GetFamilyMemberQuery, GetFamilyMemberResponse> {
  constructor(private readonly familyMemberService: FamilyMemberService) {}

  async execute(query: GetFamilyMemberQuery): Promise<GetFamilyMemberResponse> {
    const member: FamilyMember = await this.familyMemberService.findOne(
      query.id,
    );
    return new GetFamilyMemberResponse(member);
  }
}
