import { Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { IEvent, ofType, Saga } from '@nestjs/cqrs';
import { PublishFamilyMemberCommand } from '@family-member/model/publish-family-member.command';
import { FamilyMemberCreatedEvent } from '@family-member/model/family-member-created.event';
import { map } from 'rxjs/operators';

@Injectable()
export class MemberSaga {
  @Saga()
  membershipCreated = (
    events$: Observable<IEvent>,
  ): Observable<PublishFamilyMemberCommand> =>
    events$.pipe(
      ofType(FamilyMemberCreatedEvent),
      map((event: FamilyMemberCreatedEvent) => {
        return new PublishFamilyMemberCommand(event.member);
      }),
    );
}
