import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { GetFamilyMembersQuery } from '@family-member/model/get-family-members.query';
import { GetFamilyMembersResponse } from '@family-member/model/get-family-members.response';
import { FamilyMemberService } from '@family-member/family-member.service';
import { FamilyMember } from '@family-member/entities/family-member.entity';

@QueryHandler(GetFamilyMembersQuery)
export class GetFamilyMembersService
  implements IQueryHandler<GetFamilyMembersQuery, GetFamilyMembersResponse> {
  constructor(private readonly familyMemberService: FamilyMemberService) {}

  async execute(
    query: GetFamilyMembersQuery,
  ): Promise<GetFamilyMembersResponse> {
    const members: FamilyMember[] = await this.familyMemberService.findByIds(
      query.ids,
      query.offset,
      query.size,
    );
    return new GetFamilyMembersResponse(members);
  }
}
