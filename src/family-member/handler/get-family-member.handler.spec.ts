import { Test, TestingModule } from '@nestjs/testing';
import { GetFamilyMemberHandler } from './get-family-member.handler';

describe('GetFamilyMemberService', () => {
  let service: GetFamilyMemberHandler;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [GetFamilyMemberHandler],
    }).compile();

    service = module.get<GetFamilyMemberHandler>(GetFamilyMemberHandler);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
