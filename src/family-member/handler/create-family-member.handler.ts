import { CommandHandler, EventBus, ICommandHandler } from '@nestjs/cqrs';
import { CreateFamilyMemberCommand } from '@family-member/model/create-family-member.command';
import { CreateFamilyMemberResponse } from '@family-member/model/create-family-member.response';
import { FamilyMember } from '@family-member/entities/family-member.entity';
import { FamilyMemberService } from '@family-member/family-member.service';
import { FamilyMemberCreatedEvent } from '@family-member/model/family-member-created.event';

@CommandHandler(CreateFamilyMemberCommand)
export class CreateFamilyMemberHandler
  implements
    ICommandHandler<CreateFamilyMemberCommand, CreateFamilyMemberResponse> {
  constructor(
    private readonly familyMemberService: FamilyMemberService,
    private readonly eventBus: EventBus,
  ) {}

  async execute(
    command: CreateFamilyMemberCommand,
  ): Promise<CreateFamilyMemberResponse> {
    const { family, account, input } = command;

    const member = new FamilyMember();
    member.accountId = account.id;
    member.familyId = family.id;
    member.role = input.role;
    const created = await this.familyMemberService.create(member);

    const event = new FamilyMemberCreatedEvent(created);
    this.eventBus.publish(event);

    return new CreateFamilyMemberResponse(created);
  }
}
