import { Test, TestingModule } from '@nestjs/testing';
import { CreateFamilyMemberHandler } from './create-family-member.handler';

describe('CreateFamilyMemberService', () => {
  let service: CreateFamilyMemberHandler;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CreateFamilyMemberHandler],
    }).compile();

    service = module.get<CreateFamilyMemberHandler>(CreateFamilyMemberHandler);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
