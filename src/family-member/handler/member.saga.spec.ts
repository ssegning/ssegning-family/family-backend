import { Test, TestingModule } from '@nestjs/testing';
import { MemberSaga } from './member.saga';

describe('MemberService', () => {
  let service: MemberSaga;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MemberSaga],
    }).compile();

    service = module.get<MemberSaga>(MemberSaga);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
