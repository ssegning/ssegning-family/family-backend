import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { PublishFamilyMemberCommand } from '@family-member/model/publish-family-member.command';
import { RedisPubSub } from 'graphql-redis-subscriptions';
import { FamilyMember } from '@family-member/entities/family-member.entity';

@CommandHandler(PublishFamilyMemberCommand)
export class PublishFamilyMemberHandler
  implements ICommandHandler<PublishFamilyMemberCommand, void> {
  constructor(private readonly pubSub: RedisPubSub) {}

  async execute(command: PublishFamilyMemberCommand): Promise<void> {
    await this.pubSub.publish(FamilyMember.MEMBER_SUB, command.member);
  }
}
