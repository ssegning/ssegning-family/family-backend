import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { registerEnumType } from '@nestjs/graphql';

export type FamilyMemberDocument = FamilyMemberDb & Document<string>;

export enum MemberRole {
  ADMIN = 'admin',
  MEMBER = 'member',
}

export const MemberRoleArr = Object.keys(MemberRole).map(
  (key) => MemberRole[key] as MemberRole,
);

@Schema({
  collection: 'family_member',
  timestamps: true,
})
export class FamilyMemberDb {
  @Prop()
  createdAt: number;

  @Prop()
  updatedAt: number;

  @Prop({ required: true, index: true })
  accountId: string;

  @Prop({ required: true, index: true })
  familyId: string;

  @Prop({ required: true, enum: MemberRoleArr })
  role: MemberRole;
}

export const FamilyMemberSchema = SchemaFactory.createForClass(FamilyMemberDb);

registerEnumType(MemberRole, { name: 'MemberRole' });
