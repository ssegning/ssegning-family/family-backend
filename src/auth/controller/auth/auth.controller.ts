import {
  BadRequestException,
  Body,
  Controller,
  Get,
  HttpCode,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBasicAuth,
  ApiBody,
  ApiOkResponse,
  ApiOperation,
  ApiParam,
  ApiQuery,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { ValidatePasswordResponse } from '@auth/model/validate-password-response';
import { Account } from '@account/entities/account.entity';
import { CreateCredentialResponse } from '@auth/model/create-credential-response';
import { UpdateAccountCommand } from '@account/model/update-account.command';
import { RecoverPasswordResponse } from '@auth/model/recover-password-response';
import {
  ValidateRecoveryCodeInput,
  ValidateRecoveryCodeResponse,
  ValidateRecoveryTokenInput,
} from '@auth/model/validate-recovery-code';
import { DisableCredentialResponse } from '@auth/model/disable-credential-response';
import { PasswordService } from '@auth/service/password/password.service';
import { CreateAccountInput } from '@account/dto/create-account.input';
import { ValidateEmailTokenCommand } from '@auth/model/validate-email-token.command';
import { ChangePersonalDataRequest } from '@auth/model/change-personal-data-request';
import { ValidatePasswordInput } from '@auth/model/validate-password-input';
import { CredentialType } from '@auth/model/credential-type';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { CreateAccountCommand } from '@account/model/create-account.command';
import { CreateCredentialInput } from '@auth/model/create-credential-input';
import { HasAccountPassword } from '@auth/model/has-account-password';
import { GetAccountPipe } from '@shared/pipe/get-account.pipe';
import { Unprotected } from 'nest-keycloak-connect';
import { FindByAccountMap } from '@auth/model/find-by-account-map';
import { ListAccountsQuery } from '@account/model/list-accounts.query';
import { BasicAuthGuard } from '@auth/service/basic-auth/basic-auth.guard';

@Unprotected()
@UseGuards(BasicAuthGuard)
@ApiBasicAuth()
@Controller('v1/auth')
export class AuthController {
  constructor(
    private readonly passwordService: PasswordService,
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @ApiTags('accounts')
  @ApiOperation({ operationId: 'findAccountByAll' })
  @ApiQuery({ name: 'offset', type: Number })
  @ApiQuery({ name: 'size', type: Number })
  @ApiResponse({ type: [Account], status: 200 })
  @HttpCode(200)
  @Post('accounts/find/map')
  async findAccountByAll(
    @Body() input: FindByAccountMap,
    @Query() offset: string,
    @Query() size: string,
  ): Promise<Account[]> {
    console.log(input);
    const { map } = input;
    const keys = Object.keys(map);
    if (keys.length === 0) {
      const query = new ListAccountsQuery(parseInt(offset), parseInt(size));
      return await this.queryBus.execute<ListAccountsQuery, Account[]>(query);
    }
    const { search } = map;
    if (search) {
      // TODO Search by properties and return.
    }
    return []; // TODO
  }

  @ApiTags('accounts')
  @ApiOperation({ operationId: 'getAccountByIdentifier' })
  @ApiResponse({ type: Account, status: 200 })
  @ApiParam({ name: 'id', type: String })
  @HttpCode(200)
  @Get('accounts/:id')
  async getAccountByIdentifier(
    @Param('id', GetAccountPipe) account: Account,
  ): Promise<Account> {
    return account;
  }

  @ApiTags('accounts')
  @ApiOperation({ operationId: 'createAccount' })
  @ApiResponse({ type: Account, status: 201 })
  @HttpCode(201)
  @Post('accounts')
  async createAccount(@Body() input: CreateAccountInput): Promise<Account> {
    return this.commandBus.execute<CreateAccountCommand>(
      new CreateAccountCommand(input.email, input.firstName, input.lastName),
    );
  }

  @ApiTags('accounts', 'email')
  @ApiOperation({ operationId: 'validateEmail' })
  @ApiResponse({ type: null, status: 200 })
  @ApiParam({ name: 'id', type: String })
  @ApiQuery({ name: 'token', type: String })
  @HttpCode(200)
  @Get('accounts/:id/email/verify')
  async validateEmail(
    @Param('id', GetAccountPipe) account: Account,
    @Query('token') token: string,
  ): Promise<void> {
    if (account.emailVerified) {
      return;
    }
    await this.commandBus.execute(
      new ValidateEmailTokenCommand(token, account),
    );
  }

  @ApiTags('account-data')
  @ApiOperation({ operationId: 'updatePersonalData' })
  @ApiResponse({ status: 200 })
  @ApiParam({ name: 'id', type: String })
  @HttpCode(200)
  @Put('accounts/:id/personal-data')
  async updatePersonalData(
    @Param('id') accountId: string,
    @Body() request: ChangePersonalDataRequest,
  ): Promise<void> {
    const ignored: Account = await this.commandBus.execute<UpdateAccountCommand>(
      new UpdateAccountCommand(
        accountId,
        request.email,
        request.firstName,
        request.lastName,
        request.bio,
        request.phoneNumber,
        request.gender,
      ),
    );
  }

  @ApiTags('credentials')
  @ApiOperation({ operationId: 'validateCredential' })
  @ApiResponse({ type: ValidatePasswordResponse, status: 200 })
  @ApiBody({ type: ValidatePasswordInput })
  @ApiParam({ name: 'type', enum: CredentialType, type: 'string' })
  @ApiParam({
    name: 'id',
    type: String,
  })
  @HttpCode(200)
  @Post('accounts/:id/credentials/:type/verify')
  async validateCredential(
    @Param('id', GetAccountPipe) account: Account,
    @Param('type') type: CredentialType,
    @Body('challenge') challenge: string,
  ): Promise<ValidatePasswordResponse> {
    if (type !== CredentialType.PASSWORD) {
      return { valid: false };
    }

    const password = await this.passwordService.getUserLastPassword(account);
    if (!password) {
      return { valid: false };
    }

    return {
      valid: await this.passwordService.validatePassword(password, challenge),
    };
  }

  @ApiTags('credentials')
  @ApiOperation({ operationId: 'recoverPassword' })
  @ApiResponse({ status: 200, type: RecoverPasswordResponse })
  @ApiParam({ name: 'type', enum: CredentialType, type: 'string' })
  @ApiParam({ name: 'identifier', type: String })
  @HttpCode(200)
  @Post('accounts/:identifier/credentials/:type/recover')
  async recoverPassword(
    @Param('identifier', GetAccountPipe) account: Account,
    @Param('type') type: CredentialType,
  ): Promise<RecoverPasswordResponse> {
    if (type !== CredentialType.PASSWORD) {
      throw new BadRequestException('Credential error', 'Type not supported');
    }

    const {
      status,
      recovery,
      // token,
    } = await this.passwordService.recoverPassword(account);

    // TODO Send event inside CQRS
    return { code: recovery.code, status };
  }

  @ApiTags('credentials')
  @ApiBody({ type: ValidateRecoveryTokenInput })
  @ApiOperation({ operationId: 'validateRecoveryTokenPassword' })
  @ApiResponse({ status: 200, type: ValidateRecoveryCodeResponse })
  @ApiParam({ name: 'type', enum: CredentialType, type: 'string' })
  @ApiParam({
    name: 'id',
    type: String,
  })
  @HttpCode(200)
  @Post('accounts/:id/credentials/:type/validate-token')
  async validateRecoveryTokenPassword(
    @Param('id', GetAccountPipe) account: Account,
    @Param('type') type: CredentialType,
    @Body() input: ValidateRecoveryTokenInput,
  ): Promise<ValidateRecoveryCodeResponse> {
    if (type !== CredentialType.PASSWORD) {
      throw new BadRequestException('Credential error', 'Type not supported');
    }

    await this.passwordService.validateRecoveryTokenPassword(
      account,
      input.token,
    );
    return { status: true };
  }

  @ApiTags('credentials')
  @ApiBody({ type: ValidateRecoveryCodeInput })
  @ApiOperation({ operationId: 'validateRecoveryPassword' })
  @ApiResponse({ status: 200, type: ValidateRecoveryCodeResponse })
  @ApiParam({ name: 'type', enum: CredentialType, type: 'string' })
  @ApiParam({
    name: 'id',
    type: String,
  })
  @HttpCode(200)
  @Post('accounts/:id/credentials/:type/validate')
  async validateRecoveryPassword(
    @Param('id', GetAccountPipe) account: Account,
    @Param('type') type: CredentialType,
    @Body() input: ValidateRecoveryCodeInput,
  ): Promise<ValidateRecoveryCodeResponse> {
    if (type !== CredentialType.PASSWORD) {
      throw new BadRequestException('Credential error', 'Type not supported');
    }

    await this.passwordService.validateRecoveryPassword(account, input);
    return { status: true };
  }

  @ApiTags('credentials')
  @ApiOperation({ operationId: 'hasCredential' })
  @ApiOkResponse({ type: HasAccountPassword })
  @ApiParam({ name: 'type', enum: CredentialType, type: 'string' })
  @ApiParam({
    name: 'id',
    type: String,
  })
  @HttpCode(200)
  @Get('accounts/:id/credentials/:type')
  async hasCredential(
    @Param('id', GetAccountPipe) account: Account,
    @Param('type') type: CredentialType,
  ): Promise<HasAccountPassword> {
    if (type !== CredentialType.PASSWORD) {
      return { state: false };
    }
    const password = await this.passwordService.getUserLastPassword(account);
    return { state: !!password };
  }

  @ApiTags('credentials')
  @ApiOperation({ operationId: 'createCredential' })
  @ApiOkResponse({ type: CreateCredentialResponse })
  @ApiParam({ name: 'type', enum: CredentialType, type: 'string' })
  @ApiParam({
    name: 'id',
    type: String,
  })
  @HttpCode(200)
  @Post('accounts/:id/credentials/:type')
  async createCredential(
    @Param('id', GetAccountPipe) account: Account,
    @Param('type') type: CredentialType,
    @Body() input: CreateCredentialInput,
  ): Promise<CreateCredentialResponse> {
    if (type !== CredentialType.PASSWORD) {
      return { state: false };
    }
    await this.passwordService.deactivatePreviousUserPassword(account);
    await this.passwordService.createUserPassword(input.challenge, account);

    return { state: true };
  }

  @ApiTags('credentials')
  @ApiOperation({ operationId: 'disableCredential' })
  @ApiOkResponse({ type: DisableCredentialResponse })
  @ApiParam({ name: 'type', enum: CredentialType, type: 'string' })
  @ApiParam({
    name: 'id',
    type: String,
  })
  @HttpCode(200)
  @Put('accounts/:id/credentials/:type/disable')
  async deactivateUserCredentials(
    @Param('id', GetAccountPipe) account: Account,
    @Param('type') type: CredentialType,
  ): Promise<DisableCredentialResponse> {
    if (type !== CredentialType.PASSWORD) {
      return { state: false };
    }
    await this.passwordService.deactivatePreviousUserPassword(account);
    return { state: true };
  }
}
