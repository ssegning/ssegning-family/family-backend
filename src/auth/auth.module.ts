import { Module } from '@nestjs/common';
import { PasswordService } from './service/password/password.service';
import { EmailValidationService } from './service/email-validation/email-validation.service';
import { PubSubModule } from '@pub-sub/pub-sub.module';
import { CqrsModule } from '@nestjs/cqrs';
import { AuthController } from './controller/auth/auth.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { PasswordModel, PasswordSchema } from './database/password.db';
import {
  PasswordRecoverModel,
  PasswordRecoverSchema,
} from './database/password-recover.db';
import {
  EmailValidationModel,
  EmailValidationSchema,
} from './database/email-validation.db';
import { ValidateEmailTokenHandler } from './handler/validate-email-token.handler';
import { AuthSaga } from './handler/auth.saga';
import { ConfigModule } from '@nestjs/config';

@Module({
  providers: [
    AuthSaga,
    PasswordService,
    EmailValidationService,
    ValidateEmailTokenHandler,
  ],
  imports: [
    PubSubModule,
    CqrsModule,
    ConfigModule,
    MongooseModule.forFeature([
      {
        name: PasswordModel.name,
        schema: PasswordSchema,
      },
      {
        name: PasswordRecoverModel.name,
        schema: PasswordRecoverSchema,
      },
      {
        name: EmailValidationModel.name,
        schema: EmailValidationSchema,
      },
    ]),
  ],
  controllers: [AuthController],
})
export class AuthModule {}
