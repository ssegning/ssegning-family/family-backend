import { Prop, raw, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type PasswordDocument = PasswordModel & Document;

@Schema({ collection: 'account_password' })
export class PasswordModel {
  @Prop({ required: true })
  accountId: string;

  @Prop({ required: true })
  status: CredentialStatus;

  @Prop(
    raw({
      algorithm: { type: String },
      hashIterations: { type: Number },
      derivedKeySize: { type: Number },
    }),
  )
  credentialData: PasswordCredential;

  @Prop(
    raw({
      value: { type: String },
      salt: { type: String },
    }),
  )
  secretData: PasswordSecret;

  @Prop([String])
  tags: string[];
}

export const PasswordSchema = SchemaFactory.createForClass(PasswordModel);

export enum CredentialStatus {
  ACTIVE = 'active',
  LOCK = 'locked',
  DEACTIVATED = 'deactivated',
}

export interface PasswordCredential extends Record<string, string | number> {
  algorithm: string;
  hashIterations: number;
  derivedKeySize: number;
}

export interface PasswordSecret extends Record<string, string> {
  value: string;
  salt: string;
}
