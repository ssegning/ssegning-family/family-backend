import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type PasswordRecoverDocument = PasswordRecoverModel & Document;

@Schema({ collection: 'password_recovery' })
export class PasswordRecoverModel {
  @Prop({ required: true })
  accountId: string;

  @Prop({ required: true })
  status: PasswordRecoverStatus = PasswordRecoverStatus.INIT;

  @Prop({ required: true })
  code: string; // This goes by Http

  @Prop({ required: true, name: 'secret_code' })
  secret: string; // This one goes by SMS / Email

  @Prop({ required: true })
  salt: string;

  @Prop({ required: true })
  algorithm: string;

  @Prop({ required: true })
  hashIterations: number;

  @Prop({ required: true })
  derivedKeySize: number;
}

export const PasswordRecoverSchema = SchemaFactory.createForClass(
  PasswordRecoverModel,
);

export enum PasswordRecoverStatus {
  INIT = 'init',
  USED = 'used',
  PASSED = 'passed',
}
