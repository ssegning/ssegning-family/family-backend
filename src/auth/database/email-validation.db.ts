import { Prop, raw, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export interface AccountsUserEmailValidationData
  extends Record<string, string | number> {
  salt: string;
  hashIterations: number;
  derivedKeySize: number;
  algorithm: string;
}

export type EmailValidationDocument = EmailValidationModel & Document;

@Schema({ collection: 'user_email_validation' })
export class EmailValidationModel {
  accountId: string;

  @Prop({ required: true })
  code: string;

  @Prop(
    raw({
      salt: String,
      hashIterations: Number,
      derivedKeySize: Number,
      algorithm: String,
    }),
  )
  data: AccountsUserEmailValidationData;

  @Prop()
  status: EmailValidationStatus = EmailValidationStatus.OPEN;
}

export const EmailValidationSchema = SchemaFactory.createForClass(
  EmailValidationModel,
);

export enum EmailValidationStatus {
  OPEN = 'open',
  CLOSED = 'closed',
  ACHIEVED = 'achieved',
}

export const EmailValidationStatusArr = Object.keys(EmailValidationStatus).map(
  (key) => EmailValidationStatus[key] as EmailValidationStatus,
);
