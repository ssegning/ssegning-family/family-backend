import { CommandHandler, EventBus, ICommandHandler } from '@nestjs/cqrs';
import { ValidateEmailTokenCommand } from '../model/validate-email-token.command';
import { EmailValidationService } from '../service/email-validation/email-validation.service';
import { AccountEmailValidatedEvent } from '../model/account-email-validated.event';

@CommandHandler(ValidateEmailTokenCommand)
export class ValidateEmailTokenHandler
  implements ICommandHandler<ValidateEmailTokenCommand, void> {
  constructor(
    private readonly eventBus: EventBus,
    private readonly emailValidationService: EmailValidationService,
  ) {
  }

  async execute(command: ValidateEmailTokenCommand): Promise<void> {
    await this.emailValidationService.validateToken(
      command.token,
      command.account,
    );
    this.eventBus.publish(new AccountEmailValidatedEvent(command.account));
  }
}
