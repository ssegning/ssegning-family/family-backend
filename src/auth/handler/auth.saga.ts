import { ofType, Saga } from '@nestjs/cqrs';
import { Observable } from 'rxjs';
import { AccountEmailValidatedEvent } from '../model/account-email-validated.event';
import { SetEmailToValidatedCommand } from '@account/model/set-email-to-validated.command';
import { map } from 'rxjs/operators';
import { Injectable } from '@nestjs/common';

@Injectable()
export class AuthSaga {
  @Saga()
  setEmailToTrue = (
    event$: Observable<any>,
  ): Observable<SetEmailToValidatedCommand> =>
    event$.pipe(
      ofType(AccountEmailValidatedEvent),
      map(
        (event: AccountEmailValidatedEvent) =>
          new SetEmailToValidatedCommand(event.account),
      ),
    );
}
