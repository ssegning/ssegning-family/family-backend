import { ApiPropertyOptional } from '@nestjs/swagger';
import {
  IsEmail,
  IsEnum,
  IsPhoneNumber,
  IsString,
  ValidateIf,
} from 'class-validator';
import {
  AccountUserGender,
  AccountUserGenderArr,
} from '@account/database/account.db';

export class ChangePersonalDataRequest {
  @ApiPropertyOptional()
  @ValidateIf((o: ChangePersonalDataRequest) => !!o.email)
  @IsEmail()
  email: string;

  @ApiPropertyOptional()
  @ValidateIf((o: ChangePersonalDataRequest) => !!o.firstName)
  @IsString()
  firstName: string;

  @ApiPropertyOptional()
  @ValidateIf((o: ChangePersonalDataRequest) => !!o.lastName)
  @IsString()
  lastName: string;

  @ApiPropertyOptional()
  @ValidateIf((o: ChangePersonalDataRequest) => !!o.bio)
  @IsString()
  bio: string;

  @ApiPropertyOptional()
  @ValidateIf((o: ChangePersonalDataRequest) => !!o.phoneNumber)
  @IsPhoneNumber('ZZ')
  phoneNumber: string;

  @ApiPropertyOptional({
    type: () => AccountUserGender,
    enum: AccountUserGenderArr,
  })
  @ValidateIf((o: ChangePersonalDataRequest) => !!o.gender)
  @IsEnum(AccountUserGender)
  gender: AccountUserGender;
}
