import { ApiProperty } from '@nestjs/swagger';

export class CreateCredentialResponse {

  @ApiProperty()
  state: boolean;

}
