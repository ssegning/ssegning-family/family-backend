import { ApiProperty } from '@nestjs/swagger';

export class FindByAccountMap {
  @ApiProperty({ additionalProperties: { type: 'string' } })
  map: { [key: string]: string };
}
