import { ApiProperty } from '@nestjs/swagger';

export class ValidatePasswordResponse {
  @ApiProperty()
  valid: boolean;
}
