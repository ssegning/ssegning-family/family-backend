import { IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class ValidateRecoveryCodeInput {
  @ApiProperty()
  @IsString()
  code: string;

  @ApiProperty()
  @IsString()
  secret: string;
}

export class ValidateRecoveryTokenInput {
  @ApiProperty()
  @IsString()
  token: string;
}

export class ValidateRecoveryCodeResponse {
  @ApiProperty()
  status: boolean;
}
