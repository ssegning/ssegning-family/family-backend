import { ApiProperty } from '@nestjs/swagger';

abstract class CodeResponse {
  @ApiProperty()
  code: string;

  @ApiProperty()
  status: 'old' | 'new';
}

export class RecoverPasswordResponse extends CodeResponse {
}

export class RegistrationTanResponse extends CodeResponse {
}

export class RequestTanResponse extends CodeResponse {
}
