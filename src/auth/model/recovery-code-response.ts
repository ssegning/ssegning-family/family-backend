import { PasswordRecoverModel } from '../database/password-recover.db';

export interface RecoveryCodeResponse {
  recovery: PasswordRecoverModel;
  status: 'old' | 'new';
  token: string;
}
