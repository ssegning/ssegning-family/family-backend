import { IEvent } from '@nestjs/cqrs';
import { Account } from '@account/entities/account.entity';

export class AccountEmailValidatedEvent implements IEvent {
  constructor(public readonly account: Account) {
  }
}
