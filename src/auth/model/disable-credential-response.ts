import { ApiProperty } from '@nestjs/swagger';

export class DisableCredentialResponse {
  @ApiProperty()
  state: boolean;
}
