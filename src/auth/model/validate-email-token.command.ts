import { ICommand } from '@nestjs/cqrs';
import { Account } from '@account/entities/account.entity';

export class ValidateEmailTokenCommand implements ICommand {
  constructor(
    public readonly token: string,
    public readonly account: Account,
  ) {
  }
}
