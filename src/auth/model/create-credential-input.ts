import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class CreateCredentialInput {
  @ApiProperty()
  @IsString()
  challenge: string;
}
