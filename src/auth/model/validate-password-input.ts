import { ApiProperty } from '@nestjs/swagger';

export class ValidatePasswordInput {
  @ApiProperty()
  challenge: string;
}
