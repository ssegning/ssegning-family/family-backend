import { ApiProperty } from '@nestjs/swagger';

export class HasAccountPassword {
  @ApiProperty()
  state: boolean;
}
