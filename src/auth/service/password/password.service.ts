import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import {
  CredentialStatus,
  PasswordCredential,
  PasswordDocument,
  PasswordModel,
  PasswordSecret,
} from '../../database/password.db';
import {
  PasswordRecoverDocument,
  PasswordRecoverModel,
  PasswordRecoverStatus,
} from '../../database/password-recover.db';
import { Account } from '@account/entities/account.entity';
import * as crypto from 'crypto';
import { RecoveryCodeResponse } from '../../model/recovery-code-response';
import { ValidateRecoveryCodeInput } from '../../model/validate-recovery-code';

@Injectable()
export class PasswordService {
  public static readonly DEFAULT_HASH_ITERATIONS = 27500;
  public static readonly DEFAULT_ALGORITHM = 'sha256';
  public static readonly DEFAULT_DERIVED_KEY_SIZE = 512;

  public static readonly DEFAULT_PASSWORD_CREDENTIAL: PasswordCredential = {
    hashIterations: PasswordService.DEFAULT_HASH_ITERATIONS,
    algorithm: PasswordService.DEFAULT_ALGORITHM,
    derivedKeySize: PasswordService.DEFAULT_DERIVED_KEY_SIZE,
  };

  constructor(
    @InjectModel(PasswordModel.name)
    private readonly passwordModel: Model<PasswordDocument>,
    @InjectModel(PasswordRecoverModel.name)
    private readonly recoverModel: Model<PasswordRecoverDocument>,
  ) {}

  private static getSalt(): string {
    return crypto.randomBytes(32).toString('base64');
  }

  public async createUserPassword(
    challenge: string,
    account: Account,
  ): Promise<true> {
    const password: PasswordModel = new PasswordModel();
    const secretData: PasswordSecret = {
      salt: PasswordService.getSalt(),
      value: null,
    };
    const credentialData: PasswordCredential =
      PasswordService.DEFAULT_PASSWORD_CREDENTIAL;

    const buffer = crypto.pbkdf2Sync(
      challenge,
      secretData.salt,
      credentialData.hashIterations,
      credentialData.derivedKeySize,
      credentialData.algorithm,
    );
    secretData.value = buffer.toString('base64');

    password.accountId = account.id;
    password.secretData = secretData;
    password.credentialData = credentialData;
    password.status = CredentialStatus.ACTIVE;

    const entity = new this.passwordModel(password);
    await entity.save();

    return true;
  }

  public async deactivatePreviousUserPassword(account: Account): Promise<void> {
    const previousPasswords = await this.passwordModel
      .find({
        accountId: account.id,
        status: CredentialStatus.ACTIVE,
      })
      .exec();
    for (const passwordDoc of previousPasswords) {
      passwordDoc.status = CredentialStatus.DEACTIVATED;
      await passwordDoc.save();
    }
  }

  public async getUserLastPassword(
    account: Account,
  ): Promise<PasswordDocument | null> {
    const passwords = await this.passwordModel
      .find({
        accountId: account.id,
        status: CredentialStatus.ACTIVE,
      })
      .sort({ createdAt: 'desc' })
      .exec();
    if (passwords.length > 0) {
      return passwords[0];
    }
    return null;
  }

  public async validatePassword(
    password: PasswordModel,
    challenge: string,
  ): Promise<boolean> {
    const {
      algorithm,
      hashIterations,
      derivedKeySize,
    } = password.credentialData;
    const { salt, value } = password.secretData;
    const buffer = crypto.pbkdf2Sync(
      challenge,
      salt,
      hashIterations,
      derivedKeySize,
      algorithm,
    );
    return buffer.toString('base64') === value;
  }

  public async recoverPassword(
    account: Account,
  ): Promise<RecoveryCodeResponse> {
    await this.deactivatePreviousUserPassword(account);
    await this.deactivatePreviousUserRecovery(account);
    return await this.generateUserCode(account);
  }

  public async validateRecoveryTokenPassword(
    account: Account,
    token: string,
  ): Promise<void> {
    const recovery: PasswordRecoverDocument = await this.findPreviousRecovery(
      account,
    );
    if (!recovery) {
      throw new BadRequestException('Auth error', 'Code does not exist');
    }

    const buffer = crypto.pbkdf2Sync(
      recovery.code + recovery.secret + account.id,
      recovery.salt,
      recovery.hashIterations,
      recovery.derivedKeySize,
      recovery.algorithm,
    );

    if (buffer.toString('base64') !== token) {
      throw new BadRequestException(
        'Email token does not match',
        'token.error',
      );
    }
  }

  public async validateRecoveryPassword(
    account: Account,
    input: ValidateRecoveryCodeInput,
  ): Promise<void> {
    const recovery: PasswordRecoverDocument = await this.findPreviousRecovery(
      account,
    );
    if (!recovery) {
      throw new BadRequestException('Auth error', 'Code does not exist');
    }

    if (!(recovery.code === input.code && recovery.secret === input.secret)) {
      throw new BadRequestException('Auth error', 'Code does not corresponds');
    }
  }

  private async generateUserCode(
    account: Account,
  ): Promise<RecoveryCodeResponse> {
    const prevRecovery: PasswordRecoverDocument = await this.findPreviousRecovery(
      account,
    );
    if (prevRecovery) {
      const buffer = crypto.pbkdf2Sync(
        prevRecovery.code + prevRecovery.secret + account.id,
        prevRecovery.salt,
        prevRecovery.hashIterations,
        prevRecovery.derivedKeySize,
        prevRecovery.algorithm,
      );

      return {
        status: 'old',
        recovery: prevRecovery,
        token: buffer.toString('base64'),
      };
    }

    const newRecovery = new PasswordRecoverModel();
    newRecovery.accountId = account.id;
    newRecovery.salt = PasswordService.getSalt();
    newRecovery.code = Math.random().toString(36).substring(2);
    newRecovery.secret = Math.random().toString(10).substring(13);
    newRecovery.algorithm = PasswordService.DEFAULT_ALGORITHM;
    newRecovery.hashIterations = PasswordService.DEFAULT_HASH_ITERATIONS;
    newRecovery.derivedKeySize = PasswordService.DEFAULT_DERIVED_KEY_SIZE;

    const buffer = crypto.pbkdf2Sync(
      newRecovery.code + newRecovery.secret + account.id,
      newRecovery.salt,
      newRecovery.hashIterations,
      newRecovery.derivedKeySize,
      newRecovery.algorithm,
    );
    const token = buffer.toString('base64');
    const entity = new this.recoverModel(newRecovery);

    return {
      status: 'new',
      recovery: await entity.save(),
      token,
    };
  }

  private findPreviousRecovery(
    account: Account,
  ): Promise<PasswordRecoverDocument> {
    return this.recoverModel
      .findOne({
        accountId: account.id,
        status: PasswordRecoverStatus.INIT,
      })
      .sort({ createdAt: 'desc' })
      .exec();
  }

  private async deactivatePreviousUserRecovery(account: Account) {
    const previousRecoveries: PasswordRecoverDocument[] = await this.recoverModel
      .find({
        accountId: account.id,
        status: PasswordRecoverStatus.INIT,
      })
      .exec();
    for (const recovery of previousRecoveries) {
      recovery.status = PasswordRecoverStatus.PASSED;
      await recovery.save();
    }
  }
}
