import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import {
  EmailValidationDocument,
  EmailValidationModel,
  EmailValidationStatus,
} from '../../database/email-validation.db';
import crypto from 'crypto';
import { Account } from '@account/entities/account.entity';
import * as moment from 'moment';

@Injectable()
export class EmailValidationService {
  public static readonly DEFAULT_DERIVED_KEY_SIZE = 64;
  public static readonly DEFAULT_HASH_ITERATIONS = 300;
  public static readonly DEFAULT_ALGORITHM = 'sha256';

  constructor(
    @InjectModel(EmailValidationModel.name)
    private readonly emailValModel: Model<EmailValidationDocument>,
  ) {
  }

  public async validateToken(token: string, account: Account): Promise<void> {
    const prevVal = await this.findPreviousRecovery(account);
    if (!prevVal) {
      throw new BadRequestException('Account error', 'Email token not found');
    }

    const buffer = crypto.pbkdf2Sync(
      prevVal.code + account.email,
      prevVal.data.salt,
      prevVal.data.hashIterations,
      prevVal.data.derivedKeySize,
      prevVal.data.algorithm,
    );

    if (buffer.toString('base64') !== token) {
      throw new BadRequestException('email.no-match');
    }

    prevVal.status = EmailValidationStatus.ACHIEVED;
    await prevVal.save();
  }

  private findPreviousRecovery(
    account: Account,
  ): Promise<EmailValidationDocument> {
    const date = moment().subtract(10, 'days').toDate();
    return this.emailValModel
      .findOne({
        accountId: account.id,
        status: EmailValidationStatus.OPEN,
        // TODO createdAt: MoreThanOrEqual(date),
      })
      .sort({ createdAt: 'desc' })
      .exec();
  }
}
