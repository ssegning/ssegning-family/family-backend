import {
  CanActivate,
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { BasicAuthUser } from '@auth/model/basic-auth-user';

@Injectable()
export class BasicAuthGuard implements CanActivate {
  private readonly user: string;
  private readonly pass: string;

  constructor(private readonly configService: ConfigService) {
    this.user = this.configService.get<string>('APP_BASIC_USER');
    this.pass = this.configService.get<string>('APP_BASIC_PASS');
  }

  canActivate(context: ExecutionContext): boolean {
    const request = context.switchToHttp().getRequest();
    const b64auth = (request.headers.authorization || '').split(' ')[1] || '';
    const [username, password] = Buffer.from(b64auth, 'base64')
      .toString()
      .split(':');

    const isValid = this.user === username && this.pass === password;
    if (isValid) {
      request.user = new BasicAuthUser(username);
      return true;
    }
    throw new UnauthorizedException();
  }
}
