import { BasicAuthGuard } from './basic-auth.guard';

describe('BasicAuthGuardGuard', () => {
  it('should be defined', () => {
    expect(new BasicAuthGuard()).toBeDefined();
  });
});
