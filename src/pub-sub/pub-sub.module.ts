import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { RedisOptions } from 'ioredis';
import { RedisPubSub } from 'graphql-redis-subscriptions';

@Module({
  imports: [ConfigModule],
  providers: [
    {
      provide: RedisPubSub,
      useFactory: (config: ConfigService) => {
        const connection: RedisOptions = {
          host: config.get<string>('REDIS_HOST'),
          port: config.get<number>('REDIS_PORT'),
          password: config.get<string>('REDIS_PASSWORD'),
          db: 1,
        };

        return new RedisPubSub({ connection });
      },
      inject: [ConfigService],
    },
  ],
  exports: [RedisPubSub],
})
export class PubSubModule {}
