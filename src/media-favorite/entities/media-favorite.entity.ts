import { Field, ID, ObjectType } from '@nestjs/graphql';
import { MediaFavoriteDocument } from '../database/media-favorite.db';
import { ApiProperty } from '@nestjs/swagger';

@ObjectType()
export class MediaFavorite {
  @ApiProperty()
  @Field()
  createdAt: number;

  @ApiProperty()
  @Field()
  updatedAt: number;

  @Field(() => ID)
  id: string;

  memberId: string;

  mediaId: string;

  static map(doc: MediaFavoriteDocument): MediaFavorite {
    const mf = new MediaFavorite();
    mf.memberId = doc.memberId;
    mf.id = doc.id;
    mf.mediaId = doc.mediaId;

    mf.createdAt = doc.createdAt;
    mf.updatedAt = doc.updatedAt;
    return mf;
  }
}
