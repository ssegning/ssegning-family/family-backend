import { IQuery } from '@nestjs/cqrs';

export class GetMediaFavoritesQuery implements IQuery {
  constructor(
    public readonly mediaId: string,
    public readonly offset: number,
    public readonly size: number,
  ) {}
}
