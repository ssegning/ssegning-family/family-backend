import { IQuery } from '@nestjs/cqrs';

export class GetMediaFavoriteCountQuery implements IQuery {
  constructor(public readonly mediaId: string) {}
}
