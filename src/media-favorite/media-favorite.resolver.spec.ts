import { Test, TestingModule } from '@nestjs/testing';
import { MediaFavoriteResolver } from './media-favorite.resolver';
import { MediaFavoriteService } from './services/media-favorite.service';

describe('MediaFavoriteResolver', () => {
  let resolver: MediaFavoriteResolver;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MediaFavoriteResolver, MediaFavoriteService],
    }).compile();

    resolver = module.get<MediaFavoriteResolver>(MediaFavoriteResolver);
  });

  it('should be defined', () => {
    expect(resolver).toBeDefined();
  });
});
