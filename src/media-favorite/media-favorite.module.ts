import { Module } from '@nestjs/common';
import { MediaFavoriteService } from './services/media-favorite.service';
import { MediaFavoriteResolver } from './media-favorite.resolver';
import { AlbumMediaFavoritesHandler } from './handler/album-media-favorites.handler';
import { AlbumMediaFavoriteCountHandler } from './handler/album-media-favorite-count.handler';
import { MongooseModule } from '@nestjs/mongoose';
import {
  MediaFavoriteDb,
  MediaFavoriteSchema,
} from './database/media-favorite.db';
import { CqrsModule } from '@nestjs/cqrs';
import { PubSubModule } from '@pub-sub/pub-sub.module';

@Module({
  providers: [
    MediaFavoriteResolver,
    MediaFavoriteService,
    AlbumMediaFavoritesHandler,
    AlbumMediaFavoriteCountHandler,
  ],
  imports: [
    CqrsModule,
    PubSubModule,
    MongooseModule.forFeature([
      {
        name: MediaFavoriteDb.name,
        schema: MediaFavoriteSchema,
      },
    ]),
  ],
})
export class MediaFavoriteModule {}
