import { CreateMediaFavoriteInput } from './create-media-favorite.input';
import { Field, InputType, PartialType } from '@nestjs/graphql';

@InputType()
export class UpdateMediaFavoriteInput extends PartialType(
  CreateMediaFavoriteInput,
) {
  @Field(() => String)
  id: string;
}
