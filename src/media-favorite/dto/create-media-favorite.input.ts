import { Field, ID, InputType } from '@nestjs/graphql';

@InputType()
export class CreateMediaFavoriteInput {
  @Field(() => ID)
  accountId: string;

  @Field(() => ID)
  mediaId: string;
}
