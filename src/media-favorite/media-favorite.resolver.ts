import {
  Args,
  Mutation,
  Parent,
  Query,
  ResolveField,
  Resolver,
} from '@nestjs/graphql';
import { MediaFavoriteService } from './services/media-favorite.service';
import { MediaFavorite } from './entities/media-favorite.entity';
import { CreateMediaFavoriteInput } from './dto/create-media-favorite.input';
import { UpdateMediaFavoriteInput } from './dto/update-media-favorite.input';
import { Account } from '@account/entities/account.entity';
import { GetAccountQuery } from '@account/model/get-account.query';
import { QueryBus } from '@nestjs/cqrs';
import { Media } from '@media/entities/media.entity';
import { GetMediaQuery } from '@media/model/get-media.query';
import { GetFamilyQuery } from '@family/model/get-family.query';
import { GetFamilyResponse } from '@family/model/get-family.response';
import { Family } from '@family/entities/family.entity';
import { GqlHeader } from '@shared/decorator/gql-header.decorator';
import { FAMILY_HEADER } from '@shared/constants/family-header-name';
import { GetFamilyPipe } from '@shared/pipe/get-family.pipe';
import { GetFamilyMemberQuery } from '@family-member/model/get-family-member.query';
import { GetFamilyMemberResponse } from '@family-member/model/get-family-member.response';
import { FamilyMember } from '@family-member/entities/family-member.entity';

@Resolver(() => MediaFavorite)
export class MediaFavoriteResolver {
  constructor(
    private readonly mediaFavoriteService: MediaFavoriteService,
    private readonly queryBus: QueryBus,
  ) {}

  @Mutation(() => MediaFavorite)
  createMediaFavorite(
    @Args('input') input: CreateMediaFavoriteInput,
    @GqlHeader(FAMILY_HEADER, GetFamilyPipe) family: Family,
  ): Promise<MediaFavorite> {
    return this.mediaFavoriteService.create(input);
  }

  @Query(() => [MediaFavorite], { name: 'mediaFavorites' })
  findAll(
    @GqlHeader(FAMILY_HEADER, GetFamilyPipe) family: Family,
  ): Promise<MediaFavorite[]> {
    return this.mediaFavoriteService.findAll();
  }

  @Query(() => MediaFavorite, { name: 'mediaFavorite' })
  findOne(
    @Args('id', { type: () => String }) id: string,
    @GqlHeader(FAMILY_HEADER, GetFamilyPipe) family: Family,
  ): Promise<MediaFavorite> {
    return this.mediaFavoriteService.findOne(id);
  }

  @Mutation(() => MediaFavorite)
  updateMediaFavorite(
    @Args('input') input: UpdateMediaFavoriteInput,
    @GqlHeader(FAMILY_HEADER, GetFamilyPipe) family: Family,
  ): Promise<MediaFavorite> {
    return this.mediaFavoriteService.update(input.id, input);
  }

  @Mutation(() => Boolean)
  removeMediaFavorite(
    @Args('id', { type: () => String }) id: string,
    @GqlHeader(FAMILY_HEADER, GetFamilyPipe) family: Family,
  ): Promise<true> {
    return this.mediaFavoriteService.remove(id).then(() => true);
  }

  @ResolveField(() => FamilyMember)
  async member(@Parent() mf: MediaFavorite): Promise<FamilyMember> {
    const { member } = await this.queryBus.execute<
      GetFamilyMemberQuery,
      GetFamilyMemberResponse
    >(new GetFamilyMemberQuery(mf.memberId));
    return member;
  }

  @ResolveField(() => Media)
  media(@Parent() mf: MediaFavorite): Promise<Media> {
    return this.queryBus.execute<GetMediaQuery, Media>(
      new GetMediaQuery(mf.mediaId),
    );
  }
}
