import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { GetMediaFavoritesQuery } from '../model/get-media-favorites.query';
import { MediaFavoriteService } from '../services/media-favorite.service';
import { MediaFavorite } from '../entities/media-favorite.entity';

@QueryHandler(GetMediaFavoritesQuery)
export class AlbumMediaFavoritesHandler
  implements IQueryHandler<GetMediaFavoritesQuery, MediaFavorite[]> {
  constructor(private readonly mediaFavoriteService: MediaFavoriteService) {}

  async execute(query: GetMediaFavoritesQuery): Promise<MediaFavorite[]> {
    return this.mediaFavoriteService.findForMedia(
      query.mediaId,
      query.offset,
      query.size,
    );
  }
}
