import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { MediaFavoriteService } from '../services/media-favorite.service';
import { GetMediaFavoriteCountQuery } from '../model/get-media-favorite-count.query';

@QueryHandler(GetMediaFavoriteCountQuery)
export class AlbumMediaFavoriteCountHandler
  implements IQueryHandler<GetMediaFavoriteCountQuery, number> {
  constructor(private readonly mediaFavoriteService: MediaFavoriteService) {}

  async execute(query: GetMediaFavoriteCountQuery): Promise<number> {
    return this.mediaFavoriteService.countForMedia(query.mediaId);
  }
}
