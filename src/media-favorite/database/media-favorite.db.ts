import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type MediaFavoriteDocument = MediaFavoriteDb & Document;

@Schema({
  collection: 'media_favorite',
  timestamps: true,
})
export class MediaFavoriteDb {
  @Prop()
  createdAt: number;

  @Prop()
  updatedAt: number;

  @Prop()
  memberId: string;

  @Prop()
  mediaId: string;
}

export const MediaFavoriteSchema = SchemaFactory.createForClass(
  MediaFavoriteDb,
);
