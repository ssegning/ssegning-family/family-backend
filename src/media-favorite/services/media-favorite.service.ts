import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateMediaFavoriteInput } from '../dto/create-media-favorite.input';
import { UpdateMediaFavoriteInput } from '../dto/update-media-favorite.input';
import { MediaFavorite } from '../entities/media-favorite.entity';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import {
  MediaFavoriteDocument,
  MediaFavoriteDb,
} from '../database/media-favorite.db';

@Injectable()
export class MediaFavoriteService {
  constructor(
    @InjectModel(MediaFavoriteDb.name)
    private readonly mediaFavoriteModel: Model<MediaFavoriteDocument>,
  ) {}

  async create(input: CreateMediaFavoriteInput) {
    const dto = new MediaFavoriteDb();
    dto.memberId = input.accountId;
    dto.mediaId = input.mediaId;

    const model = new this.mediaFavoriteModel(dto);
    const createdModel = await model.save();
    return MediaFavorite.map(createdModel);
  }

  async findAll() {
    const models = await this.mediaFavoriteModel.find().exec();
    return models.map(MediaFavorite.map);
  }

  async findOne(id: string) {
    const model = await this.findById(id);
    return MediaFavorite.map(model);
  }

  async update(id: string, input: UpdateMediaFavoriteInput) {
    const model = await this.findById(id);

    model.memberId = input.accountId;
    model.mediaId = input.mediaId;

    const newModel = await model.save();
    return MediaFavorite.map(newModel);
  }

  async remove(id: string): Promise<void> {
    const model = await this.findById(id);
    await model.deleteOne();
  }

  async findForMedia(
    mediaId: string,
    offset: number,
    size: number,
  ): Promise<MediaFavorite[]> {
    const list = await this.mediaFavoriteModel
      .find({ mediaId })
      .skip(offset)
      .limit(size)
      .exec();
    return list.map(MediaFavorite.map);
  }

  countForMedia(mediaId: string): Promise<number> {
    return this.mediaFavoriteModel
      .find({ mediaId })
      .estimatedDocumentCount()
      .exec();
  }

  private async findById(id: string) {
    const model = await this.mediaFavoriteModel.findById(id).exec();
    if (!model) {
      throw new NotFoundException('media-favorite.not.found');
    }
    return model;
  }
}
