import { Test, TestingModule } from '@nestjs/testing';
import { MediaFavoriteService } from './media-favorite.service';

describe('MediaFavoriteService', () => {
  let service: MediaFavoriteService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MediaFavoriteService],
    }).compile();

    service = module.get<MediaFavoriteService>(MediaFavoriteService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
