import { Module } from '@nestjs/common';
import { HealthModule } from './health/health.module';
import { DatabaseModule } from './database/database.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { AlbumModule } from '@album/album.module';
import { GraphQLModule } from '@nestjs/graphql';
import { AccountModule } from '@account/account.module';
import { AlbumFavoriteModule } from '@album-favorite/album-favorite.module';
import { CommentModule } from '@comment/comment.module';
import { MediaModule } from '@media/media.module';
import { MediaFavoriteModule } from '@media-favorite/media-favorite.module';
import { PubSubModule } from '@pub-sub/pub-sub.module';
import { AuthModule } from '@auth/auth.module';
import { MessagingModule } from '@messaging/messaging.module';
import { DocumentModule } from '@document/document.module';
import { FamilyModule } from '@family/family.module';
import { FamilyMemberModule } from '@family-member/family-member.module';
import { AuthGuard } from 'nest-keycloak-connect';
import { APP_GUARD } from '@nestjs/core';
import { KeycloakModule } from '@keycloak/keycloak.module';
import { GQLService } from '@keycloak/service/g-q-l/g-q-l.service';

@Module({
  imports: [
    ConfigModule.forRoot(),
    HealthModule,
    DatabaseModule,
    GraphQLModule.forRootAsync({
      inject: [ConfigService, GQLService],
      useFactory: (configService: ConfigService, gqlService: GQLService) => ({
        autoSchemaFile: true,
        installSubscriptionHandlers: true,
        path: '/v1/api/gql',
        subscriptions: { path: '/v1/api/sub' },
        context: async ({ req, connection, ...rest }) => {
          let newReq = req;
          if (connection) {
            newReq = connection.context;
            newReq.user = await gqlService.validateToken(newReq.authToken);
          }
          return { ...rest, req: newReq };
        },
        debug: configService.get<string>('GRAPHQL_DEBUG') === 'true',
        playground: configService.get<string>('GRAPHQL_PLAYGROUND') === 'true',
        cors: {
          credentials: true,
        },
      }),
      imports: [ConfigModule, KeycloakModule],
    }),
    AccountModule,
    AlbumModule,
    AlbumFavoriteModule,
    CommentModule,
    MediaModule,
    MediaFavoriteModule,
    PubSubModule,
    AuthModule,
    MessagingModule,
    DocumentModule,
    FamilyModule,
    FamilyMemberModule,
    KeycloakModule,
  ],
  providers: [
    {
      provide: APP_GUARD,
      useClass: AuthGuard,
    },
  ],
})
export class AppModule {}
