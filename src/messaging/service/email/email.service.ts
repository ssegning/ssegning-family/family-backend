import { Injectable } from '@nestjs/common';
import { MailerService } from '@nestjs-modules/mailer';
import { EmailTemplate } from '../../model/send-email.command';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class EmailService {
  constructor(
    private readonly mailerService: MailerService,
    private readonly configService: ConfigService,
  ) {}

  /**
   * Send an email to a specific user.
   *
   * @param from of the person who send. Can be null
   * @param to of the person
   * @param subject
   * @param template
   * @param data
   */
  async sendEmail(
    from: string,
    to: string,
    subject: string,
    template: EmailTemplate,
    data: Record<string, any>,
  ): Promise<void> {
    if (!data) {
      data = {};
    }
    data.createdAt = Date.now();
    data.frontendBaseUrl = this.configService.get('FRONTEND_BASE_URL');
    await this.mailerService.sendMail({
      to,
      from,
      subject,
      template,
      context: data,
    });
  }
}
