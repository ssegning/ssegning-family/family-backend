import { Module } from '@nestjs/common';
import { MessagingService } from './messaging.service';
import { CqrsModule } from '@nestjs/cqrs';
import { SendEmailHandler } from './handler/send-email/send-email.handler';
import { EmailService } from './service/email/email.service';
import { MailerModule } from '@nestjs-modules/mailer';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';
import { join } from 'path';

@Module({
  providers: [MessagingService, SendEmailHandler, EmailService],
  imports: [
    CqrsModule,
    ConfigModule,
    MailerModule.forRootAsync({
      useFactory: (configService: ConfigService) => {
        const mailPass = configService.get<string>('MAIL_PASS');
        const mailUser = configService.get<string>('MAIL_USER');
        return {
          // https://nodemailer.com/smtp/
          transport: {
            auth: mailPass &&
              mailUser && {
                pass: mailPass,
                user: mailUser,
              },
            secure: configService.get<string>('MAIL_TLS') === 'yes',
            host: configService.get<string>('MAIL_HOST'),
            port: Number(configService.get<number>('MAIL_PORT')),
          },
          defaults: {
            from: 'Mifa <hello@mifa.ssegning.com>',
            headers: {
              'Disposition-Notification-To': 'hello@track.ssegning.com',
            },
          },
          template: {
            dir: join(process.cwd(), 'templates/pages'),
            adapter: new HandlebarsAdapter(),
            options: {
              strict: true,
            },
          },
          options: {
            partials: {
              dir: join(process.cwd(), 'templates/partials'),
              options: {
                strict: true,
              },
            },
          },
        };
      },
      imports: [ConfigModule],
      inject: [ConfigService],
    }),
  ],
})
export class MessagingModule {}
