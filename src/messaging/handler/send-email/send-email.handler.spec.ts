import { Test, TestingModule } from '@nestjs/testing';
import { SendEmailHandler } from './send-email.handler';

describe('SendEmailService', () => {
  let service: SendEmailHandler;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SendEmailHandler],
    }).compile();

    service = module.get<SendEmailHandler>(SendEmailHandler);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
