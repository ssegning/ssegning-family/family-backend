import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { SendEmailCommand } from '../../model/send-email.command';
import { EmailService } from '../../service/email/email.service';

@CommandHandler(SendEmailCommand)
export class SendEmailHandler
  implements ICommandHandler<SendEmailCommand, void> {
  constructor(private readonly emailService: EmailService) {}

  async execute(command: SendEmailCommand): Promise<void> {
    await this.emailService.sendEmail(
      command.from,
      command.to,
      command.subject,
      command.template,
      command.data,
    );
  }
}
