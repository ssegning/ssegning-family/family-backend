import { ICommand } from '@nestjs/cqrs';

export class SendEmailCommand implements ICommand {
  /**
   * Create a command to send an email from {@link from}
   * to {@link to} with subject {@link subject} and various other
   * information.
   *
   * If {@link from} is null, then uses the system's default from
   *
   * @param from
   * @param to
   * @param subject
   * @param template
   * @param data
   */
  constructor(
    public readonly from: string = 'Mifa <hello@mifa.ssegning.com>',
    public readonly to: string,
    public readonly subject: string,
    public readonly template: EmailTemplate,
    public readonly data: Record<string, any>,
  ) {}
}

export enum EmailTemplate {
  NEW_USER = 'new-user',
}
