import { Field, InputType } from '@nestjs/graphql';
import { IsEmail, IsEnum, IsPhoneNumber, IsString } from 'class-validator';
import { AccountUserGender } from '../database/account.db';

@InputType('UpdateAccountInput')
export class UpdateAccountInput {
  @IsString()
  @Field(() => String)
  id: string;

  @IsEmail()
  @Field(() => String, { description: 'User Email' })
  email: string;

  @IsString()
  @Field(() => String, { description: "User's first name" })
  firstName: string;

  @IsString()
  @Field(() => String, { description: "User's last name" })
  lastName: string;

  @IsPhoneNumber('ZZ')
  @Field(() => String, { description: "User's phone Number" })
  phoneNumber: string;

  @IsString()
  @Field(() => String, { description: "User's bio" })
  bio: string;

  @IsEnum(AccountUserGender)
  @Field(() => AccountUserGender, { description: "User's gender" })
  gender: AccountUserGender;
}
