import { IsEmail, IsString, ValidateIf } from 'class-validator';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export class CreateAccountInput {
  @ApiProperty()
  @IsEmail()
  email: string;

  @ApiPropertyOptional()
  @ApiProperty()
  @ValidateIf((o: CreateAccountInput) => !!o.firstName)
  @IsString()
  firstName: string;

  @ApiPropertyOptional()
  @ApiProperty()
  @ValidateIf((o: CreateAccountInput) => !!o.lastName)
  @IsString()
  lastName: string;
}
