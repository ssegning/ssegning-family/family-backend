import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { Model } from 'mongoose';
import {
  AccountDb,
  AccountDocument,
  AccountUserStatus,
} from '../database/account.db';
import { Account, AccountAvatar } from '../entities/account.entity';
import { isDefined } from 'class-validator';

@Injectable()
export class AccountService {
  constructor(
    @InjectModel(AccountDb.name)
    private readonly accountModel: Model<AccountDocument>,
  ) {}

  async create(input: Account): Promise<Account> {
    if (!input.status) {
      input.status = AccountUserStatus.ALLOWED;
    }
    if (!input.avatar) {
      input.avatar = new AccountAvatar(
        'https://storage.ssegning.com/family/blank-avatar.png',
        512,
        512,
      );
    }
    if (!input.bio) {
      input.bio = "Hi! It's me!";
    }
    const model = new this.accountModel(input);
    const createdModel = await model.save();
    return Account.map(createdModel);
  }

  async findAll(offset = 0, limit = 200): Promise<Account[]> {
    const models = await this.accountModel
      .find()
      .skip(offset)
      .limit(limit)
      .exec();
    return models.map(Account.map);
  }

  async findOne(id: string): Promise<Account> {
    const model = await this.findById(id);
    return Account.map(model);
  }

  async update(id: string, input: Account) {
    const model = await this.findById(id);
    model.email = input.email || model.email;
    model.firstName = input.firstName || model.firstName;
    model.lastName = input.lastName || model.lastName;
    model.avatar = input.avatar || model.avatar;

    model.email = input.email || model.email;
    model.emailVerified = isDefined(input.emailVerified)
      ? input.emailVerified
      : model.emailVerified;
    model.gender = input.gender || model.gender;
    model.status = input.status || model.status;

    model.phoneNumber = input.phoneNumber || model.phoneNumber;
    model.bio = input.bio || model.bio;

    await model.save();
    return Account.map(model);
  }

  async remove(id: string): Promise<void> {
    const model = await this.findById(id);
    await model.deleteOne();
  }

  async findList(
    ids: string[],
    offset: number,
    size: number,
  ): Promise<Account[]> {
    const list = await this.accountModel
      .find()
      .where('_id')
      .in(ids.map((id) => mongoose.Types.ObjectId(id)))
      .skip(offset)
      .limit(size)
      .exec();
    return list.map(Account.map);
  }

  async findByEmail(email: string): Promise<Account> {
    const model = await this.accountModel.findOne({ email }).exec();
    if (!model) {
      throw new NotFoundException('account.not.found');
    }
    return Account.map(model);
  }

  private async findById(id: string) {
    const model = await this.accountModel.findById(id).exec();
    if (!model) {
      throw new NotFoundException('account.not.found');
    }
    return model;
  }
}
