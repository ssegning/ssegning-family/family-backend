import { Injectable } from '@nestjs/common';
import { ofType, Saga } from '@nestjs/cqrs';
import { Observable } from 'rxjs';
import { PublishAccountCommand } from '../model/publish-account.command';
import { AccountUpdatedEvent } from '../model/account-updated.event';
import { AccountCreatedEvent } from '../model/account-created.event';
import { map } from 'rxjs/operators';
import { AccountDeletedEvent } from '../model/account-deleted.event';
import { PublishDeletedAccountCommand } from '../model/publish-deleted-account.command';
import {
  EmailTemplate,
  SendEmailCommand,
} from '@messaging/model/send-email.command';

@Injectable()
export class AccountSaga {
  @Saga()
  newAccount = (events$: Observable<any>): Observable<PublishAccountCommand> =>
    events$.pipe(
      ofType(AccountCreatedEvent, AccountUpdatedEvent),
      map(
        (event: AccountCreatedEvent | AccountUpdatedEvent) =>
          new PublishAccountCommand(event.account),
      ),
    );

  @Saga()
  removedAccount = (
    events$: Observable<any>,
  ): Observable<PublishDeletedAccountCommand> =>
    events$.pipe(
      ofType(AccountDeletedEvent),
      map(
        (event: AccountDeletedEvent) =>
          new PublishDeletedAccountCommand(event.account),
      ),
    );

  @Saga()
  sendEmail = (events$: Observable<any>): Observable<SendEmailCommand> =>
    events$.pipe(
      ofType(AccountCreatedEvent),
      map((event: AccountCreatedEvent) => {
        return new SendEmailCommand(
          undefined,
          event.account.email,
          'welcome.user',
          EmailTemplate.NEW_USER,
          { account: event.account },
        );
      }),
    );
}
