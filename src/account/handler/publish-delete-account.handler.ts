import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { Account } from '../entities/account.entity';
import { RedisPubSub } from 'graphql-redis-subscriptions';
import { PublishDeletedAccountCommand } from '../model/publish-deleted-account.command';

@CommandHandler(PublishDeletedAccountCommand)
export class PublishDeleteAccountHandler
  implements ICommandHandler<PublishDeletedAccountCommand, void> {
  constructor(private readonly pubSub: RedisPubSub) {
  }

  async execute(command: PublishDeletedAccountCommand): Promise<void> {
    await this.pubSub.publish(Account.DELETE_ACCOUNT_SUB, command.account);
  }
}
