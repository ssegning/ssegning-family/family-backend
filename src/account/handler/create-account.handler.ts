import { CommandHandler, EventBus, ICommandHandler } from '@nestjs/cqrs';
import { CreateAccountCommand } from '../model/create-account.command';
import { Account } from '../entities/account.entity';
import { AccountService } from '../services/account.service';
import { AccountCreatedEvent } from '../model/account-created.event';

@CommandHandler(CreateAccountCommand)
export class CreateAccountHandler
  implements ICommandHandler<CreateAccountCommand, Account> {
  constructor(
    private readonly accountService: AccountService,
    private readonly eventBus: EventBus,
  ) {}

  async execute(command: CreateAccountCommand): Promise<Account> {
    const model = new Account();
    model.email = command.email;
    model.firstName = command.firstName;
    model.lastName = command.lastName;

    const created = await this.accountService.create(model);
    this.eventBus.publish(new AccountCreatedEvent(created));
    return created;
  }
}
