import { CommandHandler, EventBus, ICommandHandler } from '@nestjs/cqrs';
import { Account } from '../entities/account.entity';
import { AccountService } from '../services/account.service';
import { UpdateAccountCommand } from '../model/update-account.command';
import { AccountUpdatedEvent } from '../model/account-updated.event';

@CommandHandler(UpdateAccountCommand)
export class UpdateAccountHandler
  implements ICommandHandler<UpdateAccountCommand, Account> {
  constructor(
    private readonly accountService: AccountService,
    private readonly eventBus: EventBus,
  ) {}

  async execute(command: UpdateAccountCommand): Promise<Account> {
    const oldValue = await this.accountService.findOne(command.id);
    const model = new Account();
    model.email = command.email;
    model.firstName = command.firstName;
    model.lastName = command.lastName;

    model.gender = command.gender;
    model.phoneNumber = command.phoneNumber;
    model.bio = command.bio;

    const updated = await this.accountService.update(command.id, model);
    this.eventBus.publish(new AccountUpdatedEvent(oldValue, updated));
    return updated;
  }
}
