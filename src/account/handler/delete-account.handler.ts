import { CommandHandler, EventBus, ICommandHandler } from '@nestjs/cqrs';
import { DeleteAccountCommand } from '../model/delete-account.command';
import { AccountService } from '../services/account.service';
import { AccountDeletedEvent } from '../model/account-deleted.event';

@CommandHandler(DeleteAccountCommand)
export class DeleteAccountHandler
  implements ICommandHandler<DeleteAccountCommand, true> {
  constructor(
    private readonly accountService: AccountService,
    private readonly eventBus: EventBus,
  ) {
  }

  async execute(command: DeleteAccountCommand): Promise<true> {
    const account = await this.accountService.findOne(command.id);
    await this.accountService.remove(command.id);
    this.eventBus.publish(new AccountDeletedEvent(account));
    return true;
  }
}
