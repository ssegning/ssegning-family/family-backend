import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { Account } from '../entities/account.entity';
import { PublishAccountCommand } from '../model/publish-account.command';
import { RedisPubSub } from 'graphql-redis-subscriptions';

@CommandHandler(PublishAccountCommand)
export class PublishAccountHandler
  implements ICommandHandler<PublishAccountCommand, void> {
  constructor(private readonly pubSub: RedisPubSub) {
  }

  async execute(command: PublishAccountCommand): Promise<void> {
    await this.pubSub.publish(Account.ACCOUNT_SUB, command.account);
  }
}
