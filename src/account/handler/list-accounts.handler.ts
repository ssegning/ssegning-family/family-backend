import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { AccountService } from '../services/account.service';
import { Account } from '../entities/account.entity';
import { ListAccountsQuery } from '../model/list-accounts.query';

@QueryHandler(ListAccountsQuery)
export class ListAccountsHandler
  implements IQueryHandler<ListAccountsQuery, Account[]> {
  constructor(private readonly accountService: AccountService) {
  }

  execute(query: ListAccountsQuery): Promise<Account[]> {
    return this.accountService.findAll(query.offset, query.size);
  }
}
