import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { AccountService } from '../services/account.service';
import { Account } from '../entities/account.entity';
import { GetAccountsQuery } from '../model/get-accounts.query';

@QueryHandler(GetAccountsQuery)
export class GetAccountsHandler
  implements IQueryHandler<GetAccountsQuery, Account[]> {
  constructor(private readonly accountService: AccountService) {
  }

  execute(query: GetAccountsQuery): Promise<Account[]> {
    return this.accountService.findList(query.ids, query.offset, query.size);
  }
}
