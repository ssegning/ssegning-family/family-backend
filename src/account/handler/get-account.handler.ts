import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { GetAccountQuery } from '../model/get-account.query';
import { AccountService } from '../services/account.service';
import { Account } from '../entities/account.entity';

@QueryHandler(GetAccountQuery)
export class GetAccountHandler
  implements IQueryHandler<GetAccountQuery, Account> {
  constructor(private readonly accountService: AccountService) {
  }

  async execute(query: GetAccountQuery): Promise<Account> {
    if (query.byEmail) {
      return this.accountService.findByEmail(query.identifier);
    }
    return this.accountService.findOne(query.identifier);
  }
}
