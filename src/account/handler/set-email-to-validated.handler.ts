import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { Account } from '../entities/account.entity';
import { AccountService } from '../services/account.service';
import { SetEmailToValidatedCommand } from '../model/set-email-to-validated.command';

@CommandHandler(SetEmailToValidatedCommand)
export class SetEmailToValidatedHandler
  implements ICommandHandler<SetEmailToValidatedCommand, Account> {
  constructor(public readonly accountService: AccountService) {
  }

  execute(command: SetEmailToValidatedCommand): Promise<Account> {
    const account = command.account;
    account.emailVerified = true;
    return this.accountService.update(account.id, account);
  }
}
