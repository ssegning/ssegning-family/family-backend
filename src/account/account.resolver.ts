import {
  Args,
  Int,
  Mutation,
  Parent,
  Query,
  ResolveField,
  Resolver,
  Subscription,
} from '@nestjs/graphql';
import { Account } from './entities/account.entity';
import { UpdateAccountInput } from './dto/update-account.input';
import { GetAccountQuery } from './model/get-account.query';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { UpdateAccountCommand } from './model/update-account.command';
import { DeleteAccountCommand } from './model/delete-account.command';
import { RedisPubSub } from 'graphql-redis-subscriptions';
import { SignUrlQuery } from '@document/model/sign-url.query';
import { SignUrlResponse } from '@document/model/sign-url.response';

@Resolver(() => Account)
export class AccountResolver {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
    private readonly pubSub: RedisPubSub,
  ) {}

  @Query(() => Account, { name: 'account' })
  findOne(@Args('id', { type: () => String }) id: string): Promise<Account> {
    return this.queryBus.execute<GetAccountQuery, Account>(
      new GetAccountQuery(id),
    );
  }

  @Mutation(() => Account)
  updateAccount(@Args('input') input: UpdateAccountInput): Promise<Account> {
    return this.commandBus.execute<UpdateAccountCommand>(
      new UpdateAccountCommand(
        input.id,
        input.email,
        input.firstName,
        input.lastName,
        input.bio,
        input.phoneNumber,
        input.gender,
      ),
    );
  }

  @Mutation(() => Boolean)
  removeAccount(
    @Args('id', { type: () => String }) id: string,
  ): Promise<boolean> {
    return this.commandBus.execute<DeleteAccountCommand>(
      new DeleteAccountCommand(id),
    );
  }

  @Subscription(() => Account, {
    resolve: (value) => value,
    filter: (payload: Account, variables) => payload.id === variables.id,
  })
  accountUpdates(
    @Args('id', { type: () => String, nullable: false }) id: string,
  ): AsyncIterator<Account> {
    return this.pubSub.asyncIterator(Account.ACCOUNT_SUB);
  }

  @ResolveField(() => String)
  async avatarUrl(
    @Parent() account: Account,
    @Args('width', { type: () => Int, nullable: true }) width: number,
    @Args('height', { type: () => Int, nullable: true }) height: number,
  ): Promise<string> {
    let nextUrl = account.avatar.avatarUrl;
    if (width) {
      const newWidth = Math.floor(width);
      let nextHeight = height;
      if (!nextHeight) {
        nextHeight = Math.floor(
          (account.avatar.height * newWidth) / account.avatar.width,
        );
      }
      const response = await this.queryBus.execute<
        SignUrlQuery,
        SignUrlResponse
      >(new SignUrlQuery(account.avatar.avatarUrl, newWidth, nextHeight));
      nextUrl = response.url;
    }
    return nextUrl;
  }

  @ResolveField(() => Number)
  avatarHeight(@Parent() account: Account): number {
    return account.avatar.height;
  }

  @ResolveField(() => Number)
  avatarWidth(@Parent() account: Account): number {
    return account.avatar.width;
  }
}
