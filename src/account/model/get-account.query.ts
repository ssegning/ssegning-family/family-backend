export class GetAccountQuery {
  constructor(
    public readonly identifier: string,
    public readonly byEmail: boolean = false,
  ) {
  }
}
