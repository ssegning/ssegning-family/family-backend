export class GetAccountsQuery {
  constructor(
    public readonly ids: string[],
    public readonly offset: number,
    public readonly size: number,
  ) {}
}
