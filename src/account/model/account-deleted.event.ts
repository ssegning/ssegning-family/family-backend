import { IEvent } from '@nestjs/cqrs';
import { Account } from '../entities/account.entity';

export class AccountDeletedEvent implements IEvent {
  constructor(public readonly account: Account) {
  }
}
