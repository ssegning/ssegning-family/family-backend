import { IEvent } from '@nestjs/cqrs';
import { Account } from '../entities/account.entity';

export class AccountUpdatedEvent implements IEvent {
  constructor(
    public readonly oldAccount: Account,
    public readonly account: Account,
  ) {
  }
}
