import { ICommand } from '@nestjs/cqrs';
import { Account } from '../entities/account.entity';

export class SetEmailToValidatedCommand implements ICommand {
  constructor(public readonly account: Account) {
  }
}
