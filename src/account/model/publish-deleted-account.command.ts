import { Account } from '../entities/account.entity';
import { ICommand } from '@nestjs/cqrs';

export class PublishDeletedAccountCommand implements ICommand {
  constructor(public readonly account: Account) {
  }
}
