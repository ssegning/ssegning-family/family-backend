import { IQuery } from '@nestjs/cqrs';

export class ListAccountsQuery implements IQuery {
  constructor(public readonly offset: number, public readonly size: number) {
  }
}
