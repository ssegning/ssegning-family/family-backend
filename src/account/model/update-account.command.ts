import { ICommand } from '@nestjs/cqrs';
import { AccountUserGender } from '../database/account.db';

export class UpdateAccountCommand implements ICommand {
  constructor(
    public readonly id: string,
    public readonly email: string,
    public readonly firstName: string,
    public readonly lastName: string,
    public readonly bio: string,
    public readonly phoneNumber: string,
    public readonly gender: AccountUserGender,
  ) {}
}
