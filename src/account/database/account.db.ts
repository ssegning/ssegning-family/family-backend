import { Prop, raw, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { registerEnumType } from '@nestjs/graphql';

export type AccountDocument = AccountDb & Document;

@Schema({
  collection: 'account',
  timestamps: true,
})
export class AccountDb {
  @Prop()
  createdAt: number;

  @Prop()
  updatedAt: number;

  @Prop({ required: true })
  email: string;

  @Prop()
  emailVerified: boolean;

  @Prop()
  firstName: string;

  @Prop()
  lastName: string;

  @Prop(
    raw({
      avatarUrl: String,
      width: Number,
      height: Number,
    }),
  )
  avatar: Record<string, any>;

  @Prop()
  gender: AccountUserGender;

  @Prop()
  status: AccountUserStatus = AccountUserStatus.ALLOWED;

  @Prop()
  phoneNumber: string;

  @Prop()
  bio: string;
}

export const AccountSchema = SchemaFactory.createForClass(AccountDb);

export enum AccountUserGender {
  MAN = 'male',
  WOMAN = 'female',
}

export const AccountUserGenderArr = Object.keys(AccountUserGender).map(
  (key) => AccountUserGender[key] as AccountUserGender,
);

registerEnumType(AccountUserGender, { name: 'Gender' });

export enum AccountUserStatus {
  ALLOWED = 'allowed',
  BLOCKED = 'blocked',
}

export const AccountUserStatusArr = Object.keys(AccountUserStatus).map(
  (key) => AccountUserStatus[key] as AccountUserStatus,
);
