import { Field, ID, ObjectType } from '@nestjs/graphql';
import {
  AccountDocument,
  AccountUserGender,
  AccountUserGenderArr,
  AccountUserStatus,
  AccountUserStatusArr,
} from '../database/account.db';
import { ApiProperty } from '@nestjs/swagger';

export class AccountAvatar {
  @ApiProperty()
  avatarUrl: string;

  @ApiProperty()
  width: number;

  @ApiProperty()
  height: number;

  constructor(avatarUrl: string, width: number, height: number) {
    this.avatarUrl = avatarUrl;
    this.width = width;
    this.height = height;
  }
}

@ObjectType()
export class Account {
  static readonly ACCOUNT_SUB = 'account_sub';
  static readonly DELETE_ACCOUNT_SUB = 'delete_account_sub';

  @ApiProperty()
  @Field()
  createdAt: number;

  @ApiProperty()
  @Field()
  updatedAt: number;

  @ApiProperty()
  @Field(() => ID, { description: "User's ID" })
  id: string;

  @ApiProperty()
  @Field(() => String, { description: "User's first name", nullable: true })
  firstName: string;

  @ApiProperty()
  @Field(() => String, { description: "User's last name", nullable: true })
  lastName: string;

  @ApiProperty({ type: () => AccountAvatar })
  avatar: AccountAvatar;

  @ApiProperty()
  @Field(() => String, { description: "User's email" })
  email: string;

  @ApiProperty()
  emailVerified: boolean;

  @ApiProperty({
    type: () => AccountUserGender,
    enum: AccountUserGenderArr,
  })
  @Field(() => AccountUserGender, { description: "User's gender" })
  gender: AccountUserGender;

  @ApiProperty({
    type: () => AccountUserStatus,
    enum: AccountUserStatusArr,
  })
  status: AccountUserStatus;

  @ApiProperty()
  @Field(() => String, { description: "User's phone number", nullable: true })
  phoneNumber: string;

  @ApiProperty()
  @Field(() => String, { description: "User's bio", nullable: true })
  bio: string;

  static map(model: AccountDocument): Account {
    const acc = new Account();
    acc.id = model.id;
    acc.firstName = model.firstName;
    acc.lastName = model.lastName;
    acc.avatar = new AccountAvatar(
      model.avatar.avatarUrl,
      model.avatar.width,
      model.avatar.height,
    );

    acc.email = model.email;
    acc.emailVerified = model.emailVerified;
    acc.gender = model.gender;
    acc.status = model.status;

    acc.phoneNumber = model.phoneNumber;
    acc.bio = model.bio;

    acc.createdAt = model.createdAt;
    acc.updatedAt = model.updatedAt;
    return acc;
  }
}
