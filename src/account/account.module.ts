import { Module } from '@nestjs/common';
import { AccountService } from './services/account.service';
import { AccountResolver } from './account.resolver';
import { MongooseModule } from '@nestjs/mongoose';
import { AccountDb, AccountSchema } from './database/account.db';
import { GetAccountHandler } from './handler/get-account.handler';
import { GetAccountsHandler } from './handler/get-accounts.handler';
import { PubSubModule } from '@pub-sub/pub-sub.module';
import { ListAccountsHandler } from './handler/list-accounts.handler';
import { AccountSaga } from './handler/account.saga';
import { CreateAccountHandler } from './handler/create-account.handler';
import { UpdateAccountHandler } from './handler/update-account.handler';
import { PublishAccountHandler } from './handler/publish-account.handler';
import { DeleteAccountHandler } from './handler/delete-account.handler';
import { PublishDeleteAccountHandler } from './handler/publish-delete-account.handler';
import { CqrsModule } from '@nestjs/cqrs';

@Module({
  providers: [
    AccountResolver,
    AccountService,
    GetAccountHandler,
    GetAccountsHandler,
    ListAccountsHandler,
    AccountSaga,
    CreateAccountHandler,
    UpdateAccountHandler,
    PublishAccountHandler,
    DeleteAccountHandler,
    PublishDeleteAccountHandler,
  ],
  imports: [
    PubSubModule,
    CqrsModule,
    MongooseModule.forFeature([
      {
        name: AccountDb.name,
        schema: AccountSchema,
      },
    ]),
  ],
})
export class AccountModule {
}
