import { Test, TestingModule } from '@nestjs/testing';
import { AlbumFavoriteService } from './album-favorite.service';

describe('AlbumFavoriteService', () => {
  let service: AlbumFavoriteService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AlbumFavoriteService],
    }).compile();

    service = module.get<AlbumFavoriteService>(AlbumFavoriteService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
