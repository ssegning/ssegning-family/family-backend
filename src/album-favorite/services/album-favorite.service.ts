import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateAlbumFavoriteInput } from '../dto/create-album-favorite.input';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import {
  AlbumFavoriteDocument,
  AlbumFavoriteDb,
} from '../database/album-favorite.db';
import { AlbumFavorite } from '../entities/album-favorite.entity';

@Injectable()
export class AlbumFavoriteService {
  constructor(
    @InjectModel(AlbumFavoriteDb.name)
    private readonly albumFavoriteModel: Model<AlbumFavoriteDocument>,
  ) {}

  async create(input: CreateAlbumFavoriteInput): Promise<AlbumFavorite> {
    const dto = new AlbumFavoriteDb();
    dto.memberId = input.memberId;
    dto.albumId = input.albumId;

    const model = new this.albumFavoriteModel(dto);
    const createdModel: AlbumFavoriteDocument = await model.save();
    return AlbumFavorite.map(createdModel);
  }

  async findAll(memberId: string) {
    const models = await this.albumFavoriteModel.find({ memberId }).exec();
    return models.map(AlbumFavorite.map);
  }

  async findOne(id: string) {
    const model = await this.findById(id);
    return AlbumFavorite.map(model);
  }

  async remove(id: string): Promise<void> {
    const model = await this.findById(id);
    await model.deleteOne();
  }

  async findForAlbum(
    albumId: string,
    offset: number,
    size: number,
  ): Promise<AlbumFavorite[]> {
    const list = await this.albumFavoriteModel
      .find({ albumId })
      .skip(offset)
      .limit(size)
      .exec();
    return list.map(AlbumFavorite.map);
  }

  private async findById(id: string) {
    const model = await this.albumFavoriteModel.findById(id).exec();
    if (!model) {
      throw new NotFoundException('album-favorite.not.found');
    }
    return model;
  }
}
