import { Test, TestingModule } from '@nestjs/testing';
import { AlbumFavoriteResolver } from './album-favorite.resolver';
import { AlbumFavoriteService } from './services/album-favorite.service';

describe('AlbumFavoriteResolver', () => {
  let resolver: AlbumFavoriteResolver;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AlbumFavoriteResolver, AlbumFavoriteService],
    }).compile();

    resolver = module.get<AlbumFavoriteResolver>(AlbumFavoriteResolver);
  });

  it('should be defined', () => {
    expect(resolver).toBeDefined();
  });
});
