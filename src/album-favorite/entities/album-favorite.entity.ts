import { Field, ID, ObjectType } from '@nestjs/graphql';
import { AlbumFavoriteDocument } from '../database/album-favorite.db';
import { ApiProperty } from '@nestjs/swagger';

@ObjectType()
export class AlbumFavorite {
  @ApiProperty()
  @Field()
  createdAt: number;

  @ApiProperty()
  @Field()
  updatedAt: number;

  @Field(() => ID)
  id: string;

  memberId: string;

  albumId: string;

  static map(doc: AlbumFavoriteDocument): AlbumFavorite {
    const am = new AlbumFavorite();
    am.memberId = doc.memberId;
    am.albumId = doc.albumId;

    am.createdAt = doc.createdAt;
    am.updatedAt = doc.updatedAt;
    return am;
  }
}
