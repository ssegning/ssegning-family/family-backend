import { Module } from '@nestjs/common';
import { AlbumFavoriteService } from './services/album-favorite.service';
import { AlbumFavoriteResolver } from './album-favorite.resolver';
import { GetAlbumFavoritesHandler } from './handler/get-album-favorites.handler';
import { MongooseModule } from '@nestjs/mongoose';
import {
  AlbumFavoriteDb,
  AlbumFavoriteSchema,
} from './database/album-favorite.db';
import { PubSubModule } from '@pub-sub/pub-sub.module';
import { CqrsModule } from '@nestjs/cqrs';

@Module({
  providers: [
    AlbumFavoriteResolver,
    AlbumFavoriteService,
    GetAlbumFavoritesHandler,
  ],
  imports: [
    CqrsModule,
    PubSubModule,
    MongooseModule.forFeature([
      {
        name: AlbumFavoriteDb.name,
        schema: AlbumFavoriteSchema,
      },
    ]),
  ],
})
export class AlbumFavoriteModule {}
