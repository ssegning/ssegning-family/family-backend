import { Document } from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

export type AlbumFavoriteDocument = AlbumFavoriteDb & Document;

@Schema({
  collection: 'album_favorite',
  timestamps: true,
})
export class AlbumFavoriteDb {
  @Prop()
  createdAt: number;

  @Prop()
  updatedAt: number;

  @Prop()
  memberId: string;

  @Prop()
  albumId: string;
}

export const AlbumFavoriteSchema = SchemaFactory.createForClass(
  AlbumFavoriteDb,
);
