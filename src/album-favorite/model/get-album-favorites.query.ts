import { IQuery } from '@nestjs/cqrs';

export class GetAlbumFavoritesQuery implements IQuery {
  constructor(
    public readonly albumId: string,
    public readonly offset: number,
    public readonly size: number,
  ) {}
}
