import { Field, ID, InputType } from '@nestjs/graphql';

@InputType()
export class CreateAlbumFavoriteInput {
  @Field(() => ID)
  memberId: string;

  @Field(() => ID)
  albumId: string;
}
