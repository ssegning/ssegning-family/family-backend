import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { GetAlbumFavoritesQuery } from '../model/get-album-favorites.query';
import { AlbumFavorite } from '../entities/album-favorite.entity';
import { AlbumFavoriteService } from '../services/album-favorite.service';

@QueryHandler(GetAlbumFavoritesQuery)
export class GetAlbumFavoritesHandler
  implements IQueryHandler<GetAlbumFavoritesQuery, AlbumFavorite[]> {
  constructor(private readonly albumFavoriteService: AlbumFavoriteService) {}

  async execute(query: GetAlbumFavoritesQuery): Promise<AlbumFavorite[]> {
    return this.albumFavoriteService.findForAlbum(
      query.albumId,
      query.offset,
      query.size,
    );
  }
}
