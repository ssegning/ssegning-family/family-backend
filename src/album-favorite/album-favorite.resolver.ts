import {
  Args,
  ID,
  Mutation,
  Parent,
  Query,
  ResolveField,
  Resolver,
} from '@nestjs/graphql';
import { AlbumFavoriteService } from './services/album-favorite.service';
import { AlbumFavorite } from './entities/album-favorite.entity';
import { CreateAlbumFavoriteInput } from './dto/create-album-favorite.input';
import { Album } from '@album/entities/album.entity';
import { GetAlbumQuery } from '@album/model/get-album.query';
import { QueryBus } from '@nestjs/cqrs';
import { GetFamilyMemberQuery } from '@family-member/model/get-family-member.query';
import { GetFamilyMemberResponse } from '@family-member/model/get-family-member.response';
import { FamilyMember } from '@family-member/entities/family-member.entity';
import { GetFamilyMemberPipe } from '@shared/pipe/get-family-member.pipe';

@Resolver(() => AlbumFavorite)
export class AlbumFavoriteResolver {
  constructor(
    private readonly albumFavoriteService: AlbumFavoriteService,
    private readonly queryBus: QueryBus,
  ) {}

  @Mutation(() => AlbumFavorite)
  createAlbumFavorite(
    @Args('input') input: CreateAlbumFavoriteInput,
  ): Promise<AlbumFavorite> {
    return this.albumFavoriteService.create(input);
  }

  @Query(() => [AlbumFavorite], { name: 'albumFavoriteByMember' })
  findByMember(
    @Args('memberId', { type: () => ID }, GetFamilyMemberPipe)
    member: FamilyMember,
  ): Promise<AlbumFavorite[]> {
    return this.albumFavoriteService.findAll(member.id);
  }

  @Query(() => AlbumFavorite, { name: 'albumFavorite' })
  findOne(@Args('id', { type: () => ID }) id: string): Promise<AlbumFavorite> {
    return this.albumFavoriteService.findOne(id);
  }

  @Mutation(() => Boolean)
  removeAlbumFavorite(
    @Args('id', { type: () => ID }) id: string,
  ): Promise<boolean> {
    return this.albumFavoriteService.remove(id).then(() => true);
  }

  @ResolveField(() => FamilyMember)
  async member(@Parent() albumFavorite: AlbumFavorite): Promise<FamilyMember> {
    const { member } = await this.queryBus.execute<
      GetFamilyMemberQuery,
      GetFamilyMemberResponse
    >(new GetFamilyMemberQuery(albumFavorite.memberId));
    return member;
  }

  @ResolveField(() => Album)
  album(@Parent() albumFavorite: AlbumFavorite): Promise<Album> {
    return this.queryBus.execute<GetAlbumQuery, Album>(
      new GetAlbumQuery(albumFavorite.albumId),
    );
  }
}
