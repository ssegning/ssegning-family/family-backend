import { Controller, Get } from '@nestjs/common';
import {
  HealthCheck,
  HealthCheckService,
  MongooseHealthIndicator,
} from '@nestjs/terminus';
import { Unprotected } from 'nest-keycloak-connect';

@Controller('health')
export class HealthController {
  constructor(
    private readonly health: HealthCheckService,
    private readonly mongooseHealthIndicator: MongooseHealthIndicator,
  ) {}

  @Get()
  @Unprotected()
  @HealthCheck()
  check() {
    return this.health.check([
      () => this.mongooseHealthIndicator.pingCheck('db'),
    ]);
  }
}
