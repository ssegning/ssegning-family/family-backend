import { Module } from '@nestjs/common';
import { HealthController } from './health.controller';
import { TerminusModule } from '@nestjs/terminus';
import { DatabaseModule } from '../database/database.module';

@Module({
  controllers: [HealthController],
  imports: [DatabaseModule, TerminusModule],
})
export class HealthModule {
}
