import { Document } from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { registerEnumType } from '@nestjs/graphql';

export type CommentDocument = CommentDb & Document;

@Schema({
  collection: 'comment',
  timestamps: true,
})
export class CommentDb {
  @Prop()
  createdAt: number;

  @Prop()
  updatedAt: number;

  @Prop()
  memberId: string;

  @Prop()
  message: string;

  @Prop()
  ownerId: string;

  @Prop()
  type: CommentOwnerType;
}

export const CommentSchema = SchemaFactory.createForClass(CommentDb);

export enum CommentOwnerType {
  ALBUM = 'album',
  MEDIA = 'media',
}

registerEnumType(CommentOwnerType, { name: 'CommentOwnerType' });
