import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import {
  CommentDocument,
  CommentDb,
  CommentOwnerType,
} from './database/comment.db';
import { Comment } from './entities/comment.entity';

@Injectable()
export class CommentService {
  constructor(
    @InjectModel(CommentDb.name)
    private readonly commentModel: Model<CommentDocument>,
  ) {}

  async create(toSave: Comment): Promise<Comment> {
    const model = new this.commentModel(toSave);
    const createdModel = await model.save();
    return Comment.map(createdModel);
  }

  async findAll(): Promise<Comment[]> {
    const models = await this.commentModel.find().exec();
    return models.map(Comment.map);
  }

  async findOne(id: string): Promise<Comment> {
    const model = await this.findById(id);
    return Comment.map(model);
  }

  async update(id: string, message: string): Promise<Comment> {
    const model = await this.findById(id);
    model.message = message;

    const newModel = await model.save();
    return Comment.map(newModel);
  }

  async remove(id: string): Promise<void> {
    const model = await this.findById(id);
    await model.deleteOne();
  }

  async findByOwnerId(
    ownerId: string,
    ownerType: CommentOwnerType,
    offset: number,
    size: number,
  ): Promise<Comment[]> {
    const list = await this.commentModel
      .find()
      .where('ownerId', ownerId)
      .where('type', ownerType)
      .skip(offset)
      .limit(size)
      .exec();
    return list.map(Comment.map);
  }

  private async findById(id: string): Promise<CommentDocument> {
    const model = await this.commentModel.findById(id).exec();
    if (!model) {
      throw new NotFoundException('comment.not.found');
    }
    return model;
  }
}
