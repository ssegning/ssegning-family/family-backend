import { Field, ID, ObjectType } from '@nestjs/graphql';
import { CommentDocument, CommentOwnerType } from '../database/comment.db';
import { ApiProperty } from '@nestjs/swagger';

@ObjectType()
export class Comment {
  static readonly COMMENT_SUB = 'comment_sub';
  static readonly DELETED_COMMENT_SUB = 'deleted_comment_sub';

  @ApiProperty()
  @Field()
  createdAt: number;

  @ApiProperty()
  @Field()
  updatedAt: number;

  @Field(() => ID)
  id: string;

  memberId: string;
  ownerId: string;
  type: CommentOwnerType;

  @Field(() => String)
  message: string;

  static map(doc: CommentDocument): Comment {
    const cm = new Comment();
    cm.memberId = doc.memberId;
    cm.message = doc.message;
    cm.ownerId = doc.ownerId;
    cm.type = doc.type;
    cm.id = doc.id;

    cm.createdAt = doc.createdAt;
    cm.updatedAt = doc.updatedAt;
    return cm;
  }
}
