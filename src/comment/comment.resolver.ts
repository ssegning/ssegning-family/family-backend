import {
  Args,
  ID,
  Int,
  Mutation,
  Parent,
  Query,
  ResolveField,
  Resolver,
  Subscription,
} from '@nestjs/graphql';
import { Comment } from './entities/comment.entity';
import { CreateCommentInput } from './dto/create-comment.input';
import { UpdateCommentInput } from './dto/update-comment.input';
import { CommentOwnerType } from './database/comment.db';
import { Account } from '@account/entities/account.entity';
import { GetAccountQuery } from '@account/model/get-account.query';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { RedisPubSub } from 'graphql-redis-subscriptions';
import { AddCommentCommand } from './model/add-comment.command';
import { UpdateCommentCommand } from './model/update-comment.command';
import { DeleteCommentCommand } from './model/delete-comment.command';
import { GetCommentQuery } from './model/get-comment.query';
import { GetOwnerCommentsQuery } from './model/get-owner-comments.query';
import { GetFamilyMemberQuery } from '@family-member/model/get-family-member.query';
import { GetFamilyMemberResponse } from '@family-member/model/get-family-member.response';
import { FamilyMember } from '@family-member/entities/family-member.entity';

@Resolver(() => Comment)
export class CommentResolver {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
    private readonly pubSub: RedisPubSub,
  ) {}

  @Mutation(() => Comment)
  createComment(@Args('input') input: CreateCommentInput) {
    return this.commandBus.execute(
      new AddCommentCommand(
        input.memberId,
        input.message,
        input.ownerId,
        input.type,
      ),
    );
  }

  @Query(() => [Comment], { name: 'commentsByOwner' })
  findAll(
    @Args('ownerId', { type: () => ID }) ownerId: string,
    @Args('type', { type: () => CommentOwnerType }) type: CommentOwnerType,
    @Args('offset', { type: () => Int, nullable: true }) offset = 0,
    @Args('size', { type: () => Int, nullable: true }) size = 200,
  ) {
    return this.queryBus.execute<GetOwnerCommentsQuery, Comment[]>(
      new GetOwnerCommentsQuery(ownerId, type, offset, size),
    );
  }

  @Query(() => Comment, { name: 'comment' })
  findOne(@Args('id', { type: () => ID }) id: string) {
    return this.queryBus.execute(new GetCommentQuery(id));
  }

  @Mutation(() => Comment)
  updateComment(@Args('input') input: UpdateCommentInput) {
    return this.commandBus.execute(
      new UpdateCommentCommand(input.id, input.message),
    );
  }

  @Mutation(() => Boolean)
  removeComment(@Args('id', { type: () => ID }) id: string): Promise<true> {
    return this.commandBus.execute<DeleteCommentCommand>(
      new DeleteCommentCommand(id),
    );
  }

  @ResolveField(() => FamilyMember)
  async account(@Parent() comment: Comment): Promise<FamilyMember> {
    const { member } = await this.queryBus.execute<
      GetFamilyMemberQuery,
      GetFamilyMemberResponse
    >(new GetFamilyMemberQuery(comment.memberId));
    return member;
  }

  @Subscription(() => Comment, {
    resolve: (value) => value,
    filter: (payload: Comment, variables) =>
      payload.ownerId === variables.ownerId,
  })
  commentAdded(
    @Args('ownerId', { type: () => String }) ownerId: string,
  ): AsyncIterator<Comment> {
    return this.pubSub.asyncIterator(Comment.COMMENT_SUB);
  }

  @Subscription(() => Comment, {
    resolve: (value) => value,
    filter: (payload: Comment, variables) => payload.id === variables.id,
  })
  commentUpdated(
    @Args('id', { type: () => String }) id: string,
  ): AsyncIterator<Comment> {
    return this.pubSub.asyncIterator(Comment.COMMENT_SUB);
  }

  @Subscription(() => Comment)
  commentDeleted(): AsyncIterator<Comment> {
    return this.pubSub.asyncIterator(Comment.DELETED_COMMENT_SUB);
  }
}
