import { Field, ID, InputType } from '@nestjs/graphql';
import { CommentOwnerType } from '../database/comment.db';
import { IsDefined, IsNotEmpty } from 'class-validator';

@InputType()
export class CreateCommentInput {
  @IsNotEmpty()
  @Field(() => ID)
  memberId: string;

  @IsNotEmpty()
  @Field(() => String)
  message: string;

  @IsNotEmpty()
  @Field(() => ID)
  ownerId: string;

  @IsDefined()
  @Field(() => CommentOwnerType)
  type: CommentOwnerType;
}
