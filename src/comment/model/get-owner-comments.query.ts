import { IQuery } from '@nestjs/cqrs';
import { CommentOwnerType } from '../database/comment.db';

export class GetOwnerCommentsQuery implements IQuery {
  constructor(
    public readonly ownerId: string,
    public readonly type: CommentOwnerType,
    public readonly offset: number,
    public readonly limit: number,
  ) {
  }
}
