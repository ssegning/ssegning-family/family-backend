import { ICommand } from '@nestjs/cqrs';
import { Comment } from '../entities/comment.entity';

export class PublishCommentCommand implements ICommand {
  constructor(public readonly comment: Comment) {
  }
}
