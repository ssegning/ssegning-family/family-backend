import { IQuery } from '@nestjs/cqrs';

export class GetCommentQuery implements IQuery {
  constructor(public readonly id: string) {
  }
}
