import { ICommand } from '@nestjs/cqrs';
import { CommentOwnerType } from '../database/comment.db';

export class AddCommentCommand implements ICommand {
  constructor(
    public readonly memberId: string,
    public readonly message: string,
    public readonly ownerId: string,
    public readonly type: CommentOwnerType,
  ) {}
}
