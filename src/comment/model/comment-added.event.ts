import { IEvent } from '@nestjs/cqrs';
import { Comment } from '../entities/comment.entity';

export class CommentAddedEvent implements IEvent {
  constructor(public readonly comment: Comment) {
  }
}
