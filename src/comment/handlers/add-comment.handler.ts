import { CommentService } from '../comment.service';
import { CommandHandler, EventBus, ICommandHandler } from '@nestjs/cqrs';
import { AddCommentCommand } from '../model/add-comment.command';
import { Comment } from '../entities/comment.entity';
import { CommentAddedEvent } from '../model/comment-added.event';

@CommandHandler(AddCommentCommand)
export class AddCommentHandler
  implements ICommandHandler<AddCommentCommand, Comment> {
  constructor(
    private readonly commentService: CommentService,
    private readonly eventBus: EventBus,
  ) {}

  async execute(command: AddCommentCommand): Promise<Comment> {
    const dto = new Comment();
    dto.memberId = command.memberId;
    dto.message = command.message;
    dto.ownerId = command.ownerId;
    dto.type = command.type;

    const created = await this.commentService.create(dto);
    this.eventBus.publish(new CommentAddedEvent(created));
    return created;
  }
}
