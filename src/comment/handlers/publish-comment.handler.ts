import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { RedisPubSub } from 'graphql-redis-subscriptions';
import { Comment } from '../entities/comment.entity';
import { PublishCommentCommand } from '../model/publish-comment.command';

@CommandHandler(PublishCommentCommand)
export class PublishCommentHandler
  implements ICommandHandler<PublishCommentCommand> {
  constructor(private readonly pubSub: RedisPubSub) {
  }

  execute(command: PublishCommentCommand): Promise<any> {
    return this.pubSub.publish(Comment.COMMENT_SUB, command.comment);
  }
}
