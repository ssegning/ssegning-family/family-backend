import { CommentService } from '../comment.service';
import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { Comment } from '../entities/comment.entity';
import { GetCommentQuery } from '../model/get-comment.query';

@QueryHandler(GetCommentQuery)
export class GetCommentHandler
  implements IQueryHandler<GetCommentQuery, Comment> {
  constructor(private readonly commentService: CommentService) {
  }

  execute(command: GetCommentQuery): Promise<Comment> {
    return this.commentService.findOne(command.id);
  }
}
