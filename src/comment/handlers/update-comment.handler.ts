import { CommentService } from '../comment.service';
import { CommandHandler, EventBus, ICommandHandler } from '@nestjs/cqrs';
import { Comment } from '../entities/comment.entity';
import { UpdateCommentCommand } from '../model/update-comment.command';
import { CommentUpdatedEvent } from '../model/comment-updated.event';

@CommandHandler(UpdateCommentCommand)
export class UpdateCommentHandler
  implements ICommandHandler<UpdateCommentCommand, Comment> {
  constructor(
    private readonly commentService: CommentService,
    private readonly eventBus: EventBus,
  ) {
  }

  async execute(command: UpdateCommentCommand): Promise<Comment> {
    const updated = await this.commentService.update(
      command.id,
      command.message,
    );
    this.eventBus.publish(new CommentUpdatedEvent(updated));
    return updated;
  }
}
