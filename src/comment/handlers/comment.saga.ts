import { Injectable } from '@nestjs/common';
import { ofType, Saga } from '@nestjs/cqrs';
import { Observable } from 'rxjs';
import { PublishCommentCommand } from '../model/publish-comment.command';
import { CommentUpdatedEvent } from '../model/comment-updated.event';
import { CommentAddedEvent } from '../model/comment-added.event';
import { map } from 'rxjs/operators';

@Injectable()
export class CommentSaga {
  @Saga()
  newComment = (events$: Observable<any>): Observable<PublishCommentCommand> =>
    events$.pipe(
      ofType(CommentUpdatedEvent, CommentAddedEvent),
      map(
        (event: CommentUpdatedEvent | CommentAddedEvent) =>
          new PublishCommentCommand(event.comment),
      ),
    );
}
