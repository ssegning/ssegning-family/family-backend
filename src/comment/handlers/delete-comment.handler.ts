import { CommentService } from '../comment.service';
import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { Comment } from '../entities/comment.entity';
import { RedisPubSub } from 'graphql-redis-subscriptions';
import { DeleteCommentCommand } from '../model/delete-comment.command';

@CommandHandler(DeleteCommentCommand)
export class DeleteCommentHandler
  implements ICommandHandler<DeleteCommentCommand, true> {
  constructor(
    private readonly commentService: CommentService,
    private readonly pubSub: RedisPubSub,
  ) {
  }

  async execute(command: DeleteCommentCommand): Promise<true> {
    const found = await this.commentService.findOne(command.id);
    await this.commentService.remove(command.id);
    await this.pubSub.publish(Comment.DELETED_COMMENT_SUB, found);
    return true;
  }
}
