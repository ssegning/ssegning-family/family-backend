import { CommentService } from '../comment.service';
import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { Comment } from '../entities/comment.entity';
import { GetOwnerCommentsQuery } from '../model/get-owner-comments.query';

@QueryHandler(GetOwnerCommentsQuery)
export class GetOwnerCommentsHandler
  implements IQueryHandler<GetOwnerCommentsQuery, Comment[]> {
  constructor(private readonly commentService: CommentService) {}

  execute(command: GetOwnerCommentsQuery): Promise<Comment[]> {
    return this.commentService.findByOwnerId(
      command.ownerId,
      command.type,
      command.offset,
      command.limit,
    );
  }
}
