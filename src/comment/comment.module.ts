import { Module } from '@nestjs/common';
import { CommentService } from './comment.service';
import { CommentResolver } from './comment.resolver';
import { CqrsModule } from '@nestjs/cqrs';
import { MongooseModule } from '@nestjs/mongoose';
import { CommentDb, CommentSchema } from './database/comment.db';
import { PubSubModule } from '@pub-sub/pub-sub.module';
import { UpdateCommentHandler } from './handlers/update-comment.handler';
import { DeleteCommentHandler } from './handlers/delete-comment.handler';
import { GetCommentHandler } from './handlers/get-comment.handler';
import { GetOwnerCommentsHandler } from './handlers/get-owner-comments.handler';
import { AddCommentHandler } from './handlers/add-comment.handler';
import { CommentSaga } from './handlers/comment.saga';
import { PublishCommentHandler } from './handlers/publish-comment.handler';

@Module({
  providers: [
    CommentResolver,
    CommentService,
    AddCommentHandler,
    UpdateCommentHandler,
    DeleteCommentHandler,
    GetCommentHandler,
    GetOwnerCommentsHandler,
    CommentSaga,
    PublishCommentHandler,
  ],
  imports: [
    CqrsModule,
    PubSubModule,
    MongooseModule.forFeature([
      {
        name: CommentDb.name,
        schema: CommentSchema,
      },
    ]),
  ],
})
export class CommentModule {
}
