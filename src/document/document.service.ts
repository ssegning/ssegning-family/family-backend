import { Injectable } from '@nestjs/common';
import * as minio from 'minio';
import { ItemBucketMetadata } from 'minio';
import Imgproxy, { Gravity } from 'imgproxy';
import { ConfigService } from '@nestjs/config';
import { Readable as ReadableStream } from 'stream';

@Injectable()
export class DocumentService {
  private readonly bucketName: string;
  private readonly bucketBaseUrl: string;
  private readonly imgproxy: Imgproxy;

  constructor(
    private readonly client: minio.Client,
    private readonly configService: ConfigService,
  ) {
    this.bucketName = this.configService.get<string>('STORAGE_BUCKET');
    const bucketUrl = this.configService.get<string>('STORAGE_URL');
    const bucketPort = this.configService.get<string>('STORAGE_PORT');
    const bucketSsl = this.configService.get<string>('STORAGE_SSL') === 'true';
    this.bucketBaseUrl = `http://${bucketUrl}:${bucketPort}/${this.bucketName}`;
    if (bucketSsl) {
      this.bucketBaseUrl = this.bucketBaseUrl.replace('http', 'https');

      if (bucketPort === '443' || bucketPort === '') {
        this.bucketBaseUrl = this.bucketBaseUrl.replace(':' + bucketPort, '');
      }
    }
    this.imgproxy = new Imgproxy({
      baseUrl: this.configService.get<string>('IMGPROXY_BASE_URL'),
      key: this.configService.get<string>('IMGPROXY_KEY'),
      salt: this.configService.get<string>('IMGPROXY_SALT'),
      encode: true,
    });
  }

  async uploadFile(
    objectKey: string,
    stream: ReadableStream,
    size: number,
    data: Map<string, string>,
  ): Promise<string> {
    const metaData: ItemBucketMetadata = {};
    for (const key of data.keys()) {
      metaData[key] = data.get(key);
    }

    if (size > 0) {
      await this.client.putObject(
        this.bucketName,
        objectKey,
        stream,
        size,
        metaData,
      );
    } else {
      await this.client.putObject(this.bucketName, objectKey, stream, metaData);
    }

    return `${this.bucketBaseUrl}/${objectKey}`;
  }

  signUrlForImgProxy(url: string, width: number, height: number): string {
    return this.imgproxy
      .builder()
      .resize('fill')
      .gravity(Gravity.center)
      .width(width)
      .height(height)
      .format('png')
      .generateUrl(url);
  }
}
