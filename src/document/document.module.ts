import { Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import * as minio from 'minio';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { DocumentService } from './document.service';
import { UploadFileHandler } from './handler/upload-file.handler';
import { isString } from 'class-validator';
import { SignUrlHandler } from './handler/sign-url.handler';
import { GetImageDimensionHandler } from './handler/get-image-dimension.handler';

@Module({
  imports: [CqrsModule, ConfigModule],
  providers: [
    {
      provide: minio.Client,
      useFactory: async (configService: ConfigService) => {
        const bucketName = configService.get<string>('STORAGE_BUCKET');
        const regionName = configService.get<string>('STORAGE_REGION');

        const client = new minio.Client({
          endPoint: configService.get<string>('STORAGE_URL'),
          port: Number(configService.get<string>('STORAGE_PORT')),
          useSSL: configService.get<string>('STORAGE_SSL') === 'true',
          accessKey: configService.get<string>('STORAGE_ACCESS'),
          secretKey: configService.get<string>('STORAGE_SECRET'),
          region:
            isString(regionName) && regionName !== '' ? regionName : undefined,
        });

        if (!(await client.bucketExists(bucketName))) {
          await client.makeBucket(bucketName, regionName);
        }

        return client;
      },
      inject: [ConfigService],
    },
    DocumentService,
    UploadFileHandler,
    SignUrlHandler,
    GetImageDimensionHandler,
  ],
})
export class DocumentModule {}
