import { IQuery } from '@nestjs/cqrs';

export class GetDimensionResponse implements IQuery {
  constructor(
    public readonly width: number,
    public readonly height: number,
    public readonly mimeType: string,
  ) {}
}
