import { ICommand } from '@nestjs/cqrs';

export class SignUrlResponse implements ICommand {
  constructor(public readonly url: string) {}
}
