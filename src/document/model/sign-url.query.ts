import { IQuery } from '@nestjs/cqrs';

export class SignUrlQuery implements IQuery {
  constructor(
    public readonly url: string,
    public readonly width: number,
    public readonly height: number,
  ) {}
}
