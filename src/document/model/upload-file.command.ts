import { ICommand } from '@nestjs/cqrs';
import { Readable as ReadableStream } from 'stream';

export class UploadFileCommand implements ICommand {
  constructor(
    public readonly fileName: string,
    public readonly fileType: string,
    public readonly stream: ReadableStream,
    public readonly size: number = -1,
    public readonly data: Map<string, string> = new Map<string, string>(),
  ) {}
}
