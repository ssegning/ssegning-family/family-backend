import { IQuery } from '@nestjs/cqrs';

export class GetDimensionQuery implements IQuery {
  constructor(public readonly stream: string | NodeJS.ReadableStream) {}
}
