export class UploadFileResponse {
  constructor(public readonly fileUrl: string) {}
}
