import { Test, TestingModule } from '@nestjs/testing';
import { UploadFileHandler } from './upload-file.handler';

describe('UploadFileHandlerService', () => {
  let service: UploadFileHandler;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UploadFileHandler],
    }).compile();

    service = module.get<UploadFileHandler>(UploadFileHandler);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
