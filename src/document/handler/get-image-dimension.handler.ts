import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { GetDimensionQuery } from '@document/model/get-dimension.query';
import { GetDimensionResponse } from '@document/model/get-dimension.response';
import * as probe from 'probe-image-size';

@QueryHandler(GetDimensionQuery)
export class GetImageDimensionHandler
  implements IQueryHandler<GetDimensionQuery, GetDimensionResponse> {
  async execute(query: GetDimensionQuery): Promise<GetDimensionResponse> {
    let result: probe.ProbeResult;
    if (typeof query.stream === 'string') {
      result = await probe(query.stream, { retries: 2, timeout: 5000 });
    } else {
      result = await probe(query.stream);
    }
    return new GetDimensionResponse(result.width, result.height, result.type);
  }
}
