import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { UploadFileCommand } from '../model/upload-file.command';
import { UploadFileResponse } from '../model/upload-file.response';
import { DocumentService } from '../document.service';

@CommandHandler(UploadFileCommand)
export class UploadFileHandler
  implements ICommandHandler<UploadFileCommand, UploadFileResponse> {
  constructor(private readonly documentService: DocumentService) {}

  async execute(command: UploadFileCommand): Promise<UploadFileResponse> {
    const now: number = Date.now();
    const fileName = command.fileName.replace(/[^0-9-.a-zA-Z]/g, '');

    command.data.set('Content-Type', command.fileType);
    command.data.set('Content-Disposition', `inline; filename="${fileName}"`);
    command.data.set('Cache-Control', 'max-age=31536000');

    const url = await this.documentService.uploadFile(
      `media/${now}-${fileName}`,
      command.stream,
      command.size,
      command.data,
    );

    return new UploadFileResponse(url);
  }
}
