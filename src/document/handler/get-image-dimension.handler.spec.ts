import { Test, TestingModule } from '@nestjs/testing';
import { GetImageDimensionHandler } from './get-image-dimension.handler';

describe('GetImageDimensionHandlerService', () => {
  let service: GetImageDimensionHandler;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [GetImageDimensionHandler],
    }).compile();

    service = module.get<GetImageDimensionHandler>(GetImageDimensionHandler);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
