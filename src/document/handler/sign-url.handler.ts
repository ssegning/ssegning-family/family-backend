import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { SignUrlQuery } from '../model/sign-url.query';
import { SignUrlResponse } from '../model/sign-url.response';
import { DocumentService } from '../document.service';

@QueryHandler(SignUrlQuery)
export class SignUrlHandler
  implements IQueryHandler<SignUrlQuery, SignUrlResponse> {
  constructor(private readonly documentService: DocumentService) {}

  // TODO Make this url replacement better and controllable using env parameters
  async execute(command: SignUrlQuery): Promise<SignUrlResponse> {
    const s3Url = command.url.replace('https://storage.ssegning.com/', 's3://');
    const url = this.documentService.signUrlForImgProxy(
      s3Url,
      command.width,
      command.height,
    );
    return new SignUrlResponse(url);
  }
}
