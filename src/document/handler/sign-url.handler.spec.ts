import { Test, TestingModule } from '@nestjs/testing';
import { SignUrlHandler } from './sign-url.handler';

describe('SignUrlHandlerService', () => {
  let service: SignUrlHandler;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SignUrlHandler],
    }).compile();

    service = module.get<SignUrlHandler>(SignUrlHandler);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
