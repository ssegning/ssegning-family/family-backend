import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { FamilyDb, FamilyDocument } from '@family/database/family.db';
import { Family, FamilyAvatar } from '@family/entities/family.entity';
import { isNull } from 'util';

@Injectable()
export class FamilyService {
  constructor(
    @InjectModel(FamilyDb.name)
    private readonly familyModel: Model<FamilyDocument>,
  ) {}

  async create(family: Family): Promise<Family> {
    if (!family.avatar) {
      family.avatar = new FamilyAvatar(
        'https://storage.ssegning.com/family/family.jpg',
        3456,
        3577,
      );
    }
    if (!family.description) {
      family.description = `This family's space is designed to the ${family.name}'s family`;
    }
    const model = new this.familyModel(family);
    const createdModel = await model.save();
    return Family.map(createdModel);
  }

  async findAll(offset = 0, limit = 200): Promise<Family[]> {
    const models = await this.familyModel
      .find()
      .skip(offset)
      .limit(limit)
      .exec();
    return models.map(Family.map);
  }

  async findOne(id: string): Promise<Family> {
    const found = await this.findById(id);
    return Family.map(found);
  }

  async findOneByName(name: string): Promise<Family> {
    const found = await this.findByName(name);
    return Family.map(found);
  }

  async update(id: string, family: Family): Promise<Family> {
    const model = await this.findById(id);
    model.avatar = family.avatar || model.avatar;

    model.description = family.description || model.description;
    model.name = family.name || model.name;

    await model.save();
    return Family.map(model);
  }

  async remove(id: string): Promise<void> {
    const model = await this.findById(id);
    await model.deleteOne();
  }

  async removeByName(name: string): Promise<void> {
    const model = await this.findByName(name);
    await model.deleteOne();
  }

  private async findById(id: string): Promise<FamilyDocument> {
    const model = await this.familyModel.findById(id).exec();
    if (!model) {
      throw new NotFoundException('family.not.found');
    }
    return model;
  }

  private async findByName(name: string): Promise<FamilyDocument> {
    const model = await this.familyModel.findOne({ name });
    if (!model) {
      throw new NotFoundException('family.not.found');
    }
    return model;
  }
}
