import { Field, InputType } from '@nestjs/graphql';
import {
  IsNotEmpty,
  IsString,
  ValidateIf,
  ValidateNested,
} from 'class-validator';
import { FamilyAvatar } from '@family/entities/family.entity';

@InputType()
export class CreateFamilyInput {
  @IsNotEmpty()
  @Field(() => String)
  name: string;

  @IsNotEmpty()
  @Field(() => String)
  displayName: string;

  @ValidateIf((o: CreateFamilyInput) => !!o.description)
  @IsString()
  @Field(() => String, { nullable: true })
  description: string;

  @ValidateNested()
  @ValidateIf((o: CreateFamilyInput) => !!o.avatar)
  @IsString()
  @Field(() => FamilyAvatar, { nullable: true })
  avatar: FamilyAvatar;
}
