import { IQuery } from '@nestjs/cqrs';

export class GetFamilyQuery implements IQuery {
  constructor(
    public readonly familyName: string,
    public readonly byId: boolean,
  ) {}
}
