import { Family } from '@family/entities/family.entity';

export class CreateFamilyResponse {
  constructor(public readonly family: Family) {}
}
