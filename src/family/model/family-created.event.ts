import { IEvent } from '@nestjs/cqrs';
import { Family } from '@family/entities/family.entity';
import { Account } from '@account/entities/account.entity';

export class FamilyCreatedEvent implements IEvent {
  constructor(
    public readonly family: Family,
    public readonly creator: Account,
  ) {}
}
