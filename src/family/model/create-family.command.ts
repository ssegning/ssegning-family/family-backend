import { ICommand } from '@nestjs/cqrs';
import { Account } from '@account/entities/account.entity';
import { CreateFamilyInput } from '@family/dto/create-family.input';
import { UploadResponse } from '@shared/model/upload.response';

export class CreateFamilyCommand implements ICommand {
  constructor(
    public readonly input: CreateFamilyInput,
    public readonly account: Account,
    public readonly uploadResponse: UploadResponse,
  ) {}
}
