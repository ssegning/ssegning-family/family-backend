import { IQuery } from '@nestjs/cqrs';
import { Family } from '@family/entities/family.entity';

export class GetFamilyResponse implements IQuery {
  constructor(public readonly family: Family) {}
}
