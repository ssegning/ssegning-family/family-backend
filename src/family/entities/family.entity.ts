import { Field, ID, InputType, Int, ObjectType } from '@nestjs/graphql';
import { FamilyDocument } from '@family/database/family.db';
import { ApiProperty } from '@nestjs/swagger';
import { IsUrl } from 'class-validator';

@InputType('FamilyAvatarInput')
@ObjectType('FamilyAvatar')
export class FamilyAvatar implements Record<string, any> {
  @IsUrl()
  @ApiProperty()
  @Field()
  avatarUrl: string;

  @ApiProperty()
  @Field(() => Int, { nullable: true })
  width: number;

  @ApiProperty()
  @Field(() => Int, { nullable: true })
  height: number;

  constructor(avatarUrl: string, width: number, height: number) {
    this.avatarUrl = avatarUrl;
    this.width = width;
    this.height = height;
  }
}

@ObjectType()
export class Family {
  @ApiProperty()
  @Field()
  createdAt: number;

  @ApiProperty()
  @Field()
  updatedAt: number;

  @ApiProperty()
  @Field(() => ID, { description: "Family's ID" })
  id: string;

  @ApiProperty({ type: () => FamilyAvatar })
  avatar: FamilyAvatar;

  @ApiProperty()
  @Field()
  description: string;

  @ApiProperty()
  @Field()
  name: string;

  @ApiProperty()
  @Field()
  displayName: string;

  static map(doc: FamilyDocument): Family {
    const family = new Family();
    family.createdAt = doc.createdAt;
    family.updatedAt = doc.updatedAt;
    family.id = doc.id;
    family.name = doc.name;
    family.displayName = doc.displayName;
    family.description = doc.description;
    family.avatar = new FamilyAvatar(
      doc.avatar.avatarUrl,
      doc.avatar.width,
      doc.avatar.height,
    );

    return family;
  }
}
