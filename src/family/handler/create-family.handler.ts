import {
  CommandHandler,
  EventBus,
  ICommandHandler,
  QueryBus,
} from '@nestjs/cqrs';
import { CreateFamilyCommand } from '@family/model/create-family.command';
import { CreateFamilyResponse } from '@family/model/create-family.response';
import { FamilyService } from '@family/family.service';
import { Family } from '@family/entities/family.entity';
import { GetDimensionResponse } from '@document/model/get-dimension.response';
import { GetDimensionQuery } from '@document/model/get-dimension.query';
import { FamilyCreatedEvent } from '@family/model/family-created.event';

@CommandHandler(CreateFamilyCommand)
export class CreateFamilyHandler
  implements ICommandHandler<CreateFamilyCommand, CreateFamilyResponse> {
  constructor(
    private readonly familyService: FamilyService,
    private readonly queryBus: QueryBus,
    private readonly eventBus: EventBus,
  ) {}

  async execute(command: CreateFamilyCommand): Promise<CreateFamilyResponse> {
    const { account, uploadResponse, input } = command;
    const family = new Family();
    family.name = input.name;
    family.displayName = input.displayName;
    family.description = input.description;
    family.avatar = input.avatar;

    if (uploadResponse) {
      family.avatar.avatarUrl = uploadResponse.url;
      family.avatar.width = uploadResponse.width;
      family.avatar.height = uploadResponse.height;
    } else if (family.avatar?.avatarUrl) {
      const dimensionResponse: GetDimensionResponse = await this.queryBus
        .execute(new GetDimensionQuery(family.avatar?.avatarUrl))
        .catch((err) => {
          console.error(err);
        });
      if (dimensionResponse) {
        family.avatar.height = dimensionResponse.height;
        family.avatar.width = dimensionResponse.width;
      }
    }
    const created = await this.familyService.create(family);

    const createdEvent = new FamilyCreatedEvent(created, account);
    this.eventBus.publish(createdEvent);

    return new CreateFamilyResponse(created);
  }
}
