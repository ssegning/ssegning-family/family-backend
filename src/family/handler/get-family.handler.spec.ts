import { Test, TestingModule } from '@nestjs/testing';
import { GetFamilyHandler } from './get-family.handler';

describe('GetFamilyService', () => {
  let service: GetFamilyHandler;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [GetFamilyHandler],
    }).compile();

    service = module.get<GetFamilyHandler>(GetFamilyHandler);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
