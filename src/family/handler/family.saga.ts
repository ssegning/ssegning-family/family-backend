import { Injectable } from '@nestjs/common';
import { IEvent, ofType, Saga } from '@nestjs/cqrs';
import { Observable } from 'rxjs';
import { FamilyCreatedEvent } from '@family/model/family-created.event';
import { map } from 'rxjs/operators';
import { CreateFamilyMemberCommand } from '@family-member/model/create-family-member.command';
import { CreateFamilyMemberInput } from '@family-member/dto/create-family-member.input';
import { MemberRole } from '@family-member/database/family-member.db';

@Injectable()
export class FamilySaga {
  @Saga()
  addAdminToNewFamily = (
    events$: Observable<IEvent>,
  ): Observable<CreateFamilyMemberCommand> =>
    events$.pipe(
      ofType(FamilyCreatedEvent),
      map((event: FamilyCreatedEvent) => {
        const { family, creator } = event;
        const input = new CreateFamilyMemberInput();
        input.role = MemberRole.ADMIN;
        return new CreateFamilyMemberCommand(creator, input, family);
      }),
    );
}
