import { Test, TestingModule } from '@nestjs/testing';
import { CreateFamilyHandler } from './create-family.handler';

describe('CreateFamilyService', () => {
  let service: CreateFamilyHandler;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CreateFamilyHandler],
    }).compile();

    service = module.get<CreateFamilyHandler>(CreateFamilyHandler);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
