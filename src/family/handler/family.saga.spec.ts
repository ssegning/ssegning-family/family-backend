import { Test, TestingModule } from '@nestjs/testing';
import { FamilySaga } from './family.saga';

describe('SagaFamilyService', () => {
  let service: FamilySaga;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FamilySaga],
    }).compile();

    service = module.get<FamilySaga>(FamilySaga);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
