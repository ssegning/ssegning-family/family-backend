import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { GetFamilyQuery } from '@family/model/get-family.query';
import { GetFamilyResponse } from '@family/model/get-family.response';
import { FamilyService } from '@family/family.service';

@QueryHandler(GetFamilyQuery)
export class GetFamilyHandler
  implements IQueryHandler<GetFamilyQuery, GetFamilyResponse> {
  constructor(private readonly familyService: FamilyService) {}

  async execute(query: GetFamilyQuery): Promise<GetFamilyResponse> {
    const family = query.byId
      ? await this.familyService.findOne(query.familyName)
      : await this.familyService.findOneByName(query.familyName);
    return new GetFamilyResponse(family);
  }
}
