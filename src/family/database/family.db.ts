import { Prop, raw, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type FamilyDocument = FamilyDb & Document;

@Schema({
  collection: 'family',
  timestamps: true,
})
export class FamilyDb {
  @Prop()
  createdAt: number;

  @Prop()
  updatedAt: number;

  @Prop(
    raw({
      avatarUrl: String,
      width: Number,
      height: Number,
    }),
  )
  avatar: Record<string, any>;

  @Prop()
  description: string;

  @Prop({ required: true, unique: true })
  name: string;

  @Prop({ required: true })
  displayName: string;
}

export const FamilySchema = SchemaFactory.createForClass(FamilyDb);
