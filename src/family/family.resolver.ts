import {
  Args,
  ID,
  Int,
  Mutation,
  Parent,
  Query,
  ResolveField,
  Resolver,
} from '@nestjs/graphql';
import { FamilyService } from './family.service';
import { Family } from './entities/family.entity';
import { CreateFamilyInput } from './dto/create-family.input';
import { UpdateFamilyInput } from './dto/update-family.input';
import { SignUrlQuery } from '@document/model/sign-url.query';
import { SignUrlResponse } from '@document/model/sign-url.response';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { UseInterceptors } from '@nestjs/common';
import { GraphqlFileFieldsInterceptor } from '@shared/interceptor/upload.interceptor';
import { GraphQLUpload } from 'apollo-server-express';
import { UploadResponse } from '@shared/model/upload.response';
import { Account } from '@account/entities/account.entity';
import { GetAccountPipe } from '@shared/pipe/get-account.pipe';
import { CreateFamilyCommand } from '@family/model/create-family.command';
import { CreateFamilyResponse } from '@family/model/create-family.response';
import { GqlAccountId } from '@shared/decorator/gql-request.decorator';
import { GetFamilyPipe } from '@shared/pipe/get-family.pipe';

@Resolver(() => Family)
export class FamilyResolver {
  constructor(
    private readonly familyService: FamilyService,
    private readonly queryBus: QueryBus,
    private readonly commandBus: CommandBus,
  ) {}

  private static newHeight(family: Family, newWidth: number) {
    return Math.floor((family.avatar.height * newWidth) / family.avatar.width);
  }

  @UseInterceptors(GraphqlFileFieldsInterceptor(['file']))
  @Mutation(() => Family)
  async createFamily(
    @Args('input') input: CreateFamilyInput,
    @Args({ name: 'file', type: () => GraphQLUpload, nullable: true })
    uploadResponse: UploadResponse,
    @GqlAccountId(GetAccountPipe) account: Account,
  ): Promise<Family> {
    const command = new CreateFamilyCommand(input, account, uploadResponse);
    const response: CreateFamilyResponse = await this.commandBus.execute(
      command,
    );
    return response.family;
  }

  @Query(() => [Family], { name: 'families' })
  findAll() {
    return this.familyService.findAll();
  }

  @Query(() => Family, { name: 'family' })
  findOne(
    @Args('id', { type: () => ID }, GetFamilyPipe) family: Family,
  ): Family {
    return family;
  }

  @Mutation(() => Family)
  updateFamily(
    @Args('input') input: UpdateFamilyInput,
    @GqlAccountId(GetAccountPipe) account: Account,
  ) {
    const family = new Family();
    family.name = input.name;
    family.description = input.description;
    family.avatar = input.avatar;
    return this.familyService.update(family.id, family);
  }

  @Mutation(() => Family)
  removeFamily(@Args('id', { type: () => ID }) id: string) {
    return this.familyService.remove(id);
  }

  @ResolveField(() => String)
  async avatarUrl(
    @Parent() family: Family,
    @Args('width', { type: () => Int, nullable: true }) width: number,
    @Args('height', { type: () => Int, nullable: true }) height: number,
  ): Promise<string> {
    let nextUrl = family.avatar.avatarUrl;
    if (width) {
      const newWidth = Math.floor(width);
      let nextHeight = height;
      if (!nextHeight) {
        nextHeight = FamilyResolver.newHeight(family, newWidth);
      }
      const response = await this.queryBus.execute<
        SignUrlQuery,
        SignUrlResponse
      >(new SignUrlQuery(family.avatar.avatarUrl, newWidth, nextHeight));
      nextUrl = response.url;
    }
    return nextUrl;
  }
}
