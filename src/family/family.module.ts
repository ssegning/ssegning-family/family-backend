import { Module } from '@nestjs/common';
import { FamilyService } from './family.service';
import { FamilyResolver } from './family.resolver';
import { PubSubModule } from '@pub-sub/pub-sub.module';
import { CqrsModule } from '@nestjs/cqrs';
import { MongooseModule } from '@nestjs/mongoose';
import { FamilyDb, FamilySchema } from './database/family.db';
import { GetFamilyHandler } from './handler/get-family.handler';
import { CreateFamilyHandler } from './handler/create-family.handler';
import { FamilySaga } from './handler/family.saga';

@Module({
  providers: [
    FamilyResolver,
    FamilyService,
    GetFamilyHandler,
    CreateFamilyHandler,
    FamilySaga,
  ],
  imports: [
    PubSubModule,
    CqrsModule,
    MongooseModule.forFeature([
      {
        name: FamilyDb.name,
        schema: FamilySchema,
      },
    ]),
  ],
})
export class FamilyModule {}
