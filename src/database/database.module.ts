import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigModule, ConfigService } from '@nestjs/config';

@Module({
  imports: [
    MongooseModule.forRootAsync({
      inject: [ConfigService],
      useFactory: (c: ConfigService) => {
        const mongohost = c.get('MONGO_HOST');
        const mongoport = c.get('MONGO_PORT');
        const mongoname = c.get('MONGO_NAME');
        return {
          useUnifiedTopology: true,
          auth: {
            user: c.get<string>('MONGO_USER'),
            password: c.get<string>('MONGO_PASS'),
          },
          uri: `mongodb://${mongohost}:${mongoport}/${mongoname}`,
          useCreateIndex: true,
        };
      },
      imports: [ConfigModule],
    }),
  ],
})
export class DatabaseModule {}
